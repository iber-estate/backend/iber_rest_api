FROM python:3.11

WORKDIR /srv
COPY ./src .

# for example, you can pass "--dev" here
ARG EXTRA_PIPENV_ARGS
ARG DEBIAN_FRONTEND=noninteractive

RUN set -x \
  && apt-get update \
  && apt-get install -y gdal-bin \
  && apt-get clean -y \
  && pip install pipenv \
  && pipenv install $EXTRA_PIPENV_ARGS --deploy --system --ignore-pipfile
