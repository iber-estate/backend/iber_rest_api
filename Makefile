nope:
	$(error Invalid target)

check-env-%:
	@ if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; \
		exit 1; \
	fi

up:
	docker compose up -d

down:
	docker compose down

deploy:
	git pull && make restart

restart:
	docker compose restart app celery

stop:
	docker compose stop

shell:
	docker compose exec app ./manage.py shell

migrate:
	docker compose exec app make django_migrate

admin:
	docker compose exec app ./manage.py createsuperuser

test:
	docker compose exec app make test

test-nomigrations:
	docker compose exec app pytest --disable-warnings --nomigrations
