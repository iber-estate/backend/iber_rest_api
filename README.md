### Running app local with docker

#### Start and superuser creation
- build and start development containers
```
❯ make up
```
- create superuser
```
❯ make admin
```

### API documentation
- swagger schema
```
http://127.0.0.1:8000/api/v1/swagger/
```
- redoc schema
```
http://127.0.0.1:8000/api/v1/redoc/
```

### Service-Dependencies
- PostgreSQL
- Redis
- Celery


### Helpful commands
- run unittest
```
❯ make test
```
- launch django shell
```
❯ make shell
```
- upload the database dump
```
❯ docker compose exec -T db psql -U postgres < db.dump
```
