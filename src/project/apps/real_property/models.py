from django.core.validators import MinValueValidator, MaxValueValidator, FileExtensionValidator
from django.db import models
from django.contrib.auth import get_user_model

from project.apps.address.models import Address, Country
from project.apps.core.models import CreatedDeletedModel, CreatedModel
from project.apps.property_complex.models import PropertyComplex


def upload_to(instance, filename):
    return f'real_property/{instance.real_property.pk}/{filename}'


class Amenities(models.Model):
    """
    Amenities data model.
    """
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Tag(models.Model):
    """
    Tag data model.
    """
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class OwnershipType(models.Model):
    """
    Amenities data model.
    """
    name = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, related_name='ownership_types')

    def __str__(self):
        return self.name


class RealProperty(CreatedDeletedModel):
    """
    RealProperty data model.
    """

    class Status(models.TextChoices):
        DRAFT = 'draft'
        PUBLISHED = 'published'
        ARCHIVED = 'archived'

    class PropertyType(models.TextChoices):
        APARTMENT = 'apartment'
        VILLA = 'villa'
        TOWNHOUSE = 'townhouse'

    property_complex = models.ForeignKey(PropertyComplex, on_delete=models.PROTECT, related_name='real_property')
    address = models.ForeignKey(Address, on_delete=models.PROTECT, related_name='real_property')
    year_built = models.DateField()
    name = models.CharField(max_length=64)
    description = models.TextField()
    plot_area = models.PositiveIntegerField(null=True, blank=True)
    house_area = models.PositiveIntegerField()
    bathrooms = models.PositiveSmallIntegerField()
    bedrooms = models.PositiveSmallIntegerField()
    floor = models.PositiveSmallIntegerField(blank=True, null=True)
    waterview = models.BooleanField(default=False)
    distance_to_sea = models.PositiveIntegerField(validators=[MaxValueValidator(10000000)], blank=True, null=True)
    ownership_type = models.ForeignKey(OwnershipType, on_delete=models.PROTECT)
    status = models.CharField(max_length=64, choices=Status.choices, default=Status.PUBLISHED)
    property_type = models.CharField(max_length=64, choices=PropertyType.choices)
    sales_term = models.TextField(blank=True)
    price = models.PositiveIntegerField()
    commission = models.DecimalField(
        validators=[MinValueValidator(0.1), MaxValueValidator(100.0)], max_digits=4, decimal_places=1)
    price_list_url = models.URLField(blank=True, null=True)
    creator = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='real_property')
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    amenities = models.ManyToManyField(Amenities,  related_name='real_property')
    tags = models.ManyToManyField(Tag, related_name='real_property', blank=True)
    ownership_expired_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name


class Photo(CreatedDeletedModel):
    """
    RealProperty Photo model.
    """
    real_property = models.ForeignKey(RealProperty, on_delete=models.CASCADE, related_name='photos')
    photo = models.ImageField(upload_to=upload_to)
    order = models.PositiveSmallIntegerField()

    @property
    def photo_url(self):
        return self.photo.url

    class Meta:
        unique_together = ["order", "real_property"]


class Video(CreatedDeletedModel):
    """
    RealProperty Video model.
    """
    real_property = models.ForeignKey(RealProperty, on_delete=models.CASCADE, related_name='videos')
    video = models.FileField(
        upload_to=upload_to, null=True,
        validators=[FileExtensionValidator(allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv'])])
    external_url = models.TextField(blank=True)

    @property
    def video_url(self):
        return self.video.url if self.video else self.external_url


class Document(CreatedDeletedModel):
    """
    RealProperty Document model.
    """
    real_property = models.ForeignKey(RealProperty, on_delete=models.CASCADE, related_name='documents')
    file = models.FileField(upload_to=upload_to, null=True)
    external_url = models.TextField(blank=True)
    description = models.TextField()

    @property
    def document_url(self):
        return self.file.url if self.file else self.external_url


class SavedProperty(CreatedModel):
    real_property = models.ForeignKey(RealProperty, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='saved_property')
