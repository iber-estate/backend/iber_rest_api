from django.contrib import admin
from .models import RealProperty, Photo


class InlinePhoto(admin.TabularInline):
    model = Photo


@admin.register(RealProperty)
class RealPropertyAdmin(admin.ModelAdmin):
    inlines = [InlinePhoto]
    list_display = ('name', 'property_complex', 'creator', 'created_at', 'updated_at')
    list_filter = ('property_complex__address__district__city',)
    search_fields = ["id", "name", "updated_at"]
