# Generated by Django 5.0.2 on 2024-03-06 13:35

import django.core.validators
import django.db.models.deletion
import project.apps.real_property.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('address', '0001_initial'),
        ('property_complex', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Amenities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='OwnershipType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('description', models.TextField(blank=True)),
                ('country', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ownership_types', to='address.country')),
            ],
        ),
        migrations.CreateModel(
            name='RealProperty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('deleted_at', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='deleted at')),
                ('year_built', models.DateField()),
                ('name', models.CharField(max_length=64)),
                ('description', models.TextField()),
                ('plot_area', models.PositiveIntegerField(blank=True, null=True)),
                ('house_area', models.PositiveIntegerField()),
                ('bathrooms', models.PositiveSmallIntegerField()),
                ('bedrooms', models.PositiveSmallIntegerField()),
                ('floor', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('waterview', models.BooleanField(default=False)),
                ('distance_to_sea', models.PositiveSmallIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(2000)])),
                ('status', models.CharField(choices=[('draft', 'Draft'), ('published', 'Published'), ('archived', 'Archived')], default='published', max_length=64)),
                ('property_type', models.CharField(choices=[('apartment', 'Apartment'), ('villa', 'Villa'), ('penthouse', 'Penthouse'), ('land', 'Land')], max_length=64)),
                ('sales_term', models.TextField(blank=True)),
                ('price', models.PositiveIntegerField()),
                ('commission', models.DecimalField(decimal_places=1, max_digits=4, validators=[django.core.validators.MinValueValidator(0.1), django.core.validators.MaxValueValidator(100.0)])),
                ('price_list_url', models.URLField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('ownership_expired_date', models.DateField(blank=True, null=True)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='real_property', to='address.address')),
                ('amenities', models.ManyToManyField(related_name='real_property', to='real_property.amenities')),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='real_property', to=settings.AUTH_USER_MODEL)),
                ('ownership_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='real_property.ownershiptype')),
                ('property_complex', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='real_property', to='property_complex.propertycomplex')),
                ('tags', models.ManyToManyField(blank=True, related_name='real_property', to='real_property.tag')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('deleted_at', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='deleted at')),
                ('file', models.FileField(null=True, upload_to=project.apps.real_property.models.upload_to)),
                ('external_url', models.TextField(blank=True)),
                ('description', models.TextField()),
                ('real_property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='real_property.realproperty')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('deleted_at', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='deleted at')),
                ('video', models.FileField(null=True, upload_to=project.apps.real_property.models.upload_to, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv'])])),
                ('external_url', models.TextField(blank=True)),
                ('real_property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='videos', to='real_property.realproperty')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('deleted_at', models.DateTimeField(blank=True, editable=False, null=True, verbose_name='deleted at')),
                ('photo', models.ImageField(upload_to=project.apps.real_property.models.upload_to)),
                ('order', models.PositiveSmallIntegerField()),
                ('real_property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos', to='real_property.realproperty')),
            ],
            options={
                'unique_together': {('order', 'real_property')},
            },
        ),
    ]
