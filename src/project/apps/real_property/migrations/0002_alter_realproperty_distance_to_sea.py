# Generated by Django 5.0.2 on 2024-03-21 12:35

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('real_property', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realproperty',
            name='distance_to_sea',
            field=models.PositiveIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(10000000)]),
        ),
    ]
