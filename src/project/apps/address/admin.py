from django.contrib import admin
from .models import District, Address, City, Country
from leaflet.admin import LeafletGeoAdminMixin


class DistrictModelAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_filter = ["city", "city__country"]


class AddressModelAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    list_filter = ["district", "district__city", "district__city__country"]


admin.site.register(Country)
admin.site.register(City)
admin.site.register(Address, AddressModelAdmin)
admin.site.register(District, DistrictModelAdmin)
