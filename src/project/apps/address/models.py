from django.db import models
from djgeojson.fields import MultiPolygonField, PointField

from project.apps.core.models import CreatedDeletedModel


class Country(CreatedDeletedModel):
    name = models.CharField(max_length=64)
    phone_code = models.CharField(max_length=4)

    def __str__(self):
        return self.name


class City(CreatedDeletedModel):
    name = models.CharField(max_length=64)
    country = models.ForeignKey(Country, related_name='cities', on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class District(CreatedDeletedModel):
    name = models.CharField(max_length=64)
    city = models.ForeignKey(City, related_name='districts', on_delete=models.PROTECT)
    geo_polygon = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name


class Address(CreatedDeletedModel):
    postcode = models.CharField(max_length=16, blank=True)
    district = models.ForeignKey(District, related_name='addresses', on_delete=models.PROTECT, null=True, blank=True)
    line_1 = models.CharField(max_length=1024)
    line_2 = models.CharField(max_length=1024, blank=True)
    lat = models.FloatField()
    lng = models.FloatField()
    geo_point = PointField(blank=True, null=True)

    def __str__(self):
        return self.line_1

    class Meta:
        unique_together = (('lat', 'lng',),)

    def get_address(self):
        address = f'{self.line_1}, {self.line_2}'
        return address
