import uuid

from django.db import models
from django.contrib.auth import get_user_model

from project.apps.core.models import CreatedDeletedModel
from project.apps.real_property.models import RealProperty


def upload_to(instance, filename):
    return f'listing/{instance.pk}/{filename}'


class Listing(CreatedDeletedModel):
    """
    Listing data model.
    """
    uuid = models.UUIDField(default=uuid.uuid4)
    name = models.CharField(max_length=64, blank=True)
    user = models.ForeignKey(get_user_model(), related_name='listings', on_delete=models.CASCADE)
    properties = models.ManyToManyField(RealProperty, related_name='listings', blank=True)
    description = models.CharField(max_length=2000, blank=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    image = models.ImageField(blank=True, null=True, upload_to=upload_to)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ['name', 'user', 'description']
