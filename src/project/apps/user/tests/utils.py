from django.contrib.auth import get_user_model

from factory import django, Faker


class UserFactory(django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    name = Faker('user_name')
    email = Faker('email')
