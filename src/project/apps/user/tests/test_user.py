from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from project.apps.user.tests.utils import UserFactory

User = get_user_model()


class UserSignUpTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_object = UserFactory.build()
        cls.user_saved = UserFactory.create()
        cls.client = APIClient()
        cls.signup_url = reverse('api-v1:api-root:user-list')

    def test_if_data_is_correct__then_signup_with_realtor_role(self):
        response = self.client.post(self.signup_url, {'name': self.user_object.name, 'email': self.user_object.email})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)
        new_user = User.objects.get(email=self.user_object.email)
        self.assertEqual(new_user.role, self.user_object.Role.REALTOR)
        self.assertEqual(new_user.name, self.user_object.name)

    def test_if_name_already_exists__dont_signup(self):
        response = self.client.post(self.signup_url, {'name': self.user_saved.name, 'email': self.user_saved.email})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['email'][0]), 'user with this email already exists.')
        users = User.objects.filter(email=self.user_saved.email)
        self.assertEqual(users.count(), 1)


class UserListRetrieveTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin_user = UserFactory.create(role=User.Role.ADMINISTRATOR)
        cls.customer_user = UserFactory.create(role=User.Role.CUSTOMER)
        cls.client = APIClient()
        cls.user_list_url = reverse('api-v1:api-root:user-list')
        cls.my_user_url = reverse('api-v1:api-root:user-my')

    def test_list_if_user_is_admin__get_all_users_data(self):
        self.client.force_authenticate(self.admin_user)
        response = self.client.get(self.user_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()['results']), 2)

    def test_retrieve_if_user_is_admin__get_any_user_data(self):
        self.client.force_authenticate(self.admin_user)
        response = self.client.get(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.admin_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_if_user_is_not_admin_or_anonymous__get_error(self):
        response = self.client.get(self.user_list_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.customer_user)
        response = self.client.get(self.user_list_url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_if_user_is_not_admin_or_anonymous__get_error(self):
        response = self.client.get(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.customer_user)
        response = self.client.get(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_my__get_own_user_data(self):
        self.client.force_authenticate(self.customer_user)
        response = self.client.get(self.my_user_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['email'], self.customer_user.email)


class UserUpdateTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()
        cls.admin_user = UserFactory.create(role=User.Role.ADMINISTRATOR)
        cls.customer_user = UserFactory.create(role=User.Role.CUSTOMER)
        cls.new_user_data = {'name': 'Fake Name'}

    def test_if_user_is_admin_update_another_user__get_updated_user_data(self):
        self.client.force_authenticate(self.admin_user)
        response = self.client.patch(
            reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}), data=self.new_user_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.new_user_data['name'])
        new_user = User.objects.get(email=self.customer_user.email)
        self.assertEqual(new_user.name, self.new_user_data['name'])

    def test_if_user_update_own_data__get_update_user_data(self):
        self.client.force_authenticate(self.customer_user)
        response = self.client.patch(
            reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}), data=self.new_user_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_user = User.objects.get(email=self.customer_user.email)
        self.assertEqual(new_user.name, self.new_user_data['name'])

    def test_if_user_is_not_admin_or_anonymous_update_another_user__get_error(self):
        response = self.client.patch(
            reverse('api-v1:api-root:user-detail', kwargs={'pk': self.admin_user.pk}), data=self.new_user_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.customer_user)
        response = self.client.patch(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.admin_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UserDeleteTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()
        cls.admin_user = UserFactory.create(role=User.Role.ADMINISTRATOR)
        cls.customer_user = UserFactory.create(role=User.Role.CUSTOMER)

    def test_if_user_is_admin_delete_another_user__success(self):
        self.client.force_authenticate(self.admin_user)
        response = self.client.delete(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(User.objects.non_deleted().count(), 1)

    def test_if_user_is_not_admin_or_anonymous_delete_another_user__get_error(self):
        response = self.client.delete(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.customer_user)
        response = self.client.delete(reverse('api-v1:api-root:user-detail', kwargs={'pk': self.customer_user.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(User.objects.non_deleted().count(), 2)

