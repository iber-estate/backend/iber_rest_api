from django.contrib.auth import get_user_model

from project import celery_app
from project.apps.notification.mailgun import send_email_message

User = get_user_model()


@celery_app.task
def send_auth_email_task(email: str, password: str) -> None:
    send_email_message(msg=f'Your auth password: {password}', email=email)


@celery_app.task
def send_system_notification_email_task(msg: str) -> None:
    for admin in User.objects.filter(role=User.Role.ADMINISTRATOR, system_notifications=True):
        send_email_message(msg=msg, email=admin.email, subject='System notification')
