from django.utils.translation import gettext_lazy as _

from rest_framework.exceptions import AuthenticationFailed, NotFound

from project.api.v1.exceptions import ValidationAPIError, APIError


class UserDoesNotExist(NotFound):
    default_code = 'user_not_found'
    # Translators: raw text - 'The user with credentials provided
    # does not exist.'
    default_detail = _('user_not_found',)


class InvalidActivationToken(AuthenticationFailed):
    default_code = 'invalid_activation_token'
    # Translators: raw text - 'Account access token is invalid.'
    default_detail = _('invalid_activation_token')


class InvalidResetToken(AuthenticationFailed):
    default_code = 'invalid_reset_token'
    # Translators: raw text - 'Invalid password reset token provided.'
    default_detail = _('invalid_reset_token')


class InvalidPassword(AuthenticationFailed):
    default_code = 'invalid_password'
    # Translators: raw text - 'The password provided is wrong.'
    default_detail = _('invalid_password')


class ExactPasswordForbidden(APIError):
    default_code = 'password_same_previous'
    # Translators: raw text - 'The password you provided is same as previous.'
    default_detail = _('password_same_previous')


class SuperUserAlreadyExists(APIError):
    default_code = 'user_already_exist'
    # Translators: raw text - 'User is already exist.'
    default_detail = _('user_already_exist')


class UserNameAlreadyOccupied(APIError):
    default_code = 'username_already_occupied'
    # Translators: raw text - 'This username is already occupied.'
    default_detail = _('username_already_occupied')


class AvatarSizeTooBig(APIError):
    default_code = 'avatar_too_big'
    # Translators: raw text - 'The file submitted as avatar
    # exceeds max file size limit.'
    default_detail = _('avatar_too_big')


class AlreadyAppliedForPartnership(APIError):
    default_code = 'application_already_received'
    # Translators: raw text - 'Already received application.
    # Please wait for decision.'
    default_detail = _('application_already_received')


class PartnershipApplicationDeclined(APIError):
    default_code = 'application_declined'
    # Translators: raw text - 'Partnership cannot be accepted as of now.'
    default_detail = _('application_declined')


class PartnershipUrlNotAvailable(APIError):
    default_code = 'application_not_approved'
    # Translators: raw text - 'Cannot generate partnership URL due to
    # partnership has not been approved.'
    default_detail = _('application_not_approved')


class TooManyAsyncTasks(APIError):
    default_code = 'many_tasks'
    # Translators: raw text - 'There are too many outstanding tasks waiting
    # for execution. Please try later.'
    default_detail = _('many_tasks')


class KYCVerifiedAlready(APIError):
    default_code = 'verification_already_completed'
    # Translators: raw text - 'User verification has been completed.
    # No need to redo it.'
    default_detail = _('verification_already_completed')


class CannotVerifyWithoutFullName(APIError):
    default_code = 'verification_no_fullname'
    # Translators: raw text - 'Please set full name before verification.'
    default_detail = _('verification_no_fullname')


class KYCVerificationAPI(APIError):
    default_code = 'verification_failed'
    # Translators: raw text - 'Application for user verification failed.
    # Please try later.'
    default_detail = _('verification_failed')


class KYCServiceIsNotAvailable(APIError):
    default_code = 'verification_not_available'
    # Translators: raw text - 'User verification service is not
    # available at the moment.'
    default_detail = _('verification_not_available')


class MismatchPasswords(ValidationAPIError):
    default_code = 'mismatch_passwords'
    # Translators: raw text - 'Passwords do not match.'
    default_detail = _('mismatch_passwords')
    default_field = 'password'


class UsernameRestrictedSymbolsFound(ValidationAPIError):
    default_code = 'username_restricted_symbols'
    # Translators: raw text - 'Username field can not start with number,
    # should have latin letters, numbers, hyphen, underscore or dot.'
    default_detail = _('username_restricted_symbols')
    default_field = 'username'


class UsernameUnderscoreProhibited(ValidationAPIError):
    default_code = 'username_underscore_found'
    # Translators: raw text - 'Username field can not end with underscore.'
    default_detail = _('username_underscore_found')
    default_field = 'username'


class UsernameTooShort(ValidationAPIError):
    default_code = 'username_too_short'
    # Translators: raw text - 'Username should be at least {min_length}
    # characters long.'
    default_detail = _('username_too_short')
    default_field = 'username'


class UsernameTooLong(ValidationAPIError):
    default_code = 'username_too_long'
    # Translators: raw text - 'Username should be no more than
    # {max_length} characters long.'
    default_detail = _('username_too_long')
    default_field = 'username'


class PasswordTooShort(ValidationAPIError):
    default_code = 'password_too_short'
    # Translators: raw text - 'This password is too short.
    # It must contain at least {min_length} characters.'
    default_detail = _('password_too_short')
    default_field = 'password'


class PasswordTooCommon(ValidationAPIError):
    default_code = 'password_too_common'
    # Translators: raw text - 'This password is too common.'
    default_detail = _('password_too_common')
    default_field = 'password'


class PasswordEntirelyNumeric(ValidationAPIError):
    default_code = 'password_entirely_numeric'
    # Translators: raw text - 'This password is entirely numeric.'
    default_detail = _('password_entirely_numeric')
    default_field = 'password'


class PasswordTooSimilar(ValidationAPIError):
    default_code = 'password_too_similar'
    # Translators: raw text - 'The password is too similar to the email.'
    default_detail = _('password_too_similar')
    default_field = 'password'


class PasswordHaveSpaces(ValidationAPIError):
    default_code = 'password_have_spaces'
    # Translators: raw text - 'The password cannot contain spaces.'
    default_detail = _('password_have_spaces')
    default_field = 'password'
