from django.db import models
from django.utils import timezone as tz
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from phonenumber_field.modelfields import PhoneNumberField

from project.apps.core.managers import DeletedManager
from project.apps.core.models import UserDeletedModel, CreatedModel
from project.apps.user.exceptions import SuperUserAlreadyExists


class UserManager(BaseUserManager, DeletedManager):

    def _create_user(self, name, email, password, **extra_fields):
        """
        Create and save a user with the given username and password.
        """
        user = self.model(name=name, email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(name, email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        # We will have only one superuser account (platform account)
        users = User.objects.filter(is_superuser=True)
        if users.exists():
            raise SuperUserAlreadyExists()
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('role', 'administrator')
        return self._create_user(email=email, password=password, **extra_fields)


class User(UserDeletedModel, AbstractBaseUser, PermissionsMixin):
    """
    Abstract user model.
    """

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    USERNAME_MAX_LENGTH = 64
    USERNAME_MIN_LENGTH = 5
    REQUIRED_FIELDS = ['name']

    objects = UserManager()

    class Role(models.TextChoices):
        ADMINISTRATOR = 'administrator'
        REALTOR = 'realtor'
        DEVELOPER = 'developer'
        FREELANCER = 'freelancer'
        CUSTOMER = 'customer'

    email = models.EmailField(blank=False, unique=True)
    role = models.CharField(max_length=64, choices=Role.choices, default=Role.CUSTOMER)
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True, null=True)
    phone = PhoneNumberField(unique=True, blank=True, null=True)
    wa_number = PhoneNumberField(unique=True, blank=True, null=True)
    date_joined = models.DateTimeField(default=tz.now)
    is_staff = models.BooleanField(default=False)
    new_notification = models.BooleanField(default=False)
    system_notifications = models.BooleanField(default=False)

    def __str__(self):
        return self.email


class OneTimePassword(CreatedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    password = models.CharField(max_length=64)

    @property
    def is_valid(self) -> bool:
        time_diff = tz.now() - self.created_at
        if time_diff.total_seconds() >= settings.OTP_EXPIRE_TIME:
            return False
        return True
