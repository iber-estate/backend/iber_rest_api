# Generated by Django 5.0.6 on 2024-09-26 12:28

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invest_project', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='investproject',
            name='contact_type',
            field=models.TextField(choices=[('whatsapp', 'Whatsapp'), ('telegram', 'Telegram'), ('email', 'Email'), ('phone', 'Phone')], default='email', max_length=32),
        ),
        migrations.AddField(
            model_name='investproject',
            name='display_options',
            field=models.TextField(choices=[('exclusive', 'Exclusive'), ('urgent', 'Urgent')], default='exclusive', max_length=32),
        ),
        migrations.AddField(
            model_name='investproject',
            name='goal',
            field=models.TextField(choices=[('investment', 'Investment'), ('sale', 'Sale')], default='investment', max_length=32),
        ),
        migrations.CreateModel(
            name='VideoLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('video_link', models.TextField(blank=True)),
                ('invest_project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='videos', to='invest_project.investproject')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
