from django.db import models
from django.contrib.auth import get_user_model
from phonenumber_field.modelfields import PhoneNumberField

from project.apps.address.models import Address
from project.apps.core.models import CreatedDeletedModel, CreatedModel


def upload_to(instance, filename):
    return f'invest_project/{instance.invest_project.pk}/{filename}'


class InvestCategory(models.Model):
    name = models.CharField(max_length=64)


class InvestProject(CreatedDeletedModel):

    class Status(models.TextChoices):
        DRAFT = 'draft'
        PUBLISHED = 'published'
        ARCHIVED = 'archived'

    class Goal(models.TextChoices):
        INVESTMENT = 'investment'
        SALE = 'sale'

    class DisplayOptions(models.TextChoices):
        EXCLUSIVE = 'exclusive'
        URGENT = 'urgent'

    class ContactType(models.TextChoices):
        WHATSAPP = 'whatsapp'
        TELEGRAM = 'telegram'
        EMAIL = 'email'
        PHONE = 'phone'

    name = models.CharField(max_length=256)
    user_full_name = models.CharField(max_length=64, blank=True)
    description = models.TextField()
    invest_amount = models.PositiveIntegerField()
    expected_profit = models.PositiveSmallIntegerField()
    payback_period = models.PositiveSmallIntegerField()
    financial_plan = models.URLField()
    business_plan = models.URLField()
    additional_info = models.TextField(blank=True)
    phone = PhoneNumberField(blank=True, null=True)
    category = models.ForeignKey(InvestCategory, on_delete=models.CASCADE, related_name='projects')
    goal = models.TextField(max_length=32, choices=Goal.choices, default=Goal.INVESTMENT)
    display_options = models.TextField(max_length=32, choices=DisplayOptions.choices, blank=True, null=True)
    contact_type = models.TextField(max_length=32, choices=ContactType.choices, default=ContactType.EMAIL)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='projects')
    draft_address = models.CharField(blank=True)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, related_name='projects', blank=True, null=True)
    status = models.TextField(max_length=32, choices=Status.choices, default=Status.DRAFT)


class DocumentLink(models.Model):
    """
    InvestProject DocumentLink model.
    """
    invest_project = models.ForeignKey(InvestProject, on_delete=models.CASCADE, related_name='documents')
    document_link = models.TextField(blank=True)


class PresentationLink(models.Model):
    """
    InvestProject Presentation model.
    """
    invest_project = models.ForeignKey(InvestProject, on_delete=models.CASCADE, related_name='presentations')
    presentation_link = models.TextField(blank=True)


class VideoLink(CreatedModel):
    """
    InvestProject VideoLink model.
    """
    invest_project = models.ForeignKey(InvestProject, on_delete=models.CASCADE, related_name='videos')
    video_link = models.TextField(blank=True)


class Photo(CreatedModel):
    """
    InvestProject Photo model.
    """
    invest_project = models.ForeignKey(InvestProject, on_delete=models.CASCADE, related_name='photos')
    photo = models.ImageField(upload_to=upload_to)
    order = models.PositiveSmallIntegerField()

    @property
    def photo_url(self):
        return self.photo.url

    class Meta:
        unique_together = ["order", "invest_project"]
