from django.contrib import admin
from .models import InvestCategory, InvestProject

admin.site.register(InvestCategory)
admin.site.register(InvestProject)
