from django.db import models
from django.contrib.auth import get_user_model

from project.apps.core.models import CreatedModel, CreatedDeletedModel


class Agency(CreatedDeletedModel):
    """
    Agency data model.
    """
    name = models.CharField(max_length=64, unique=True)
    email = models.EmailField(blank=False, null=True)
    website = models.URLField(blank=True, null=True)
    description = models.TextField()
    employees = models.ManyToManyField(get_user_model(), related_name='agency', through='agencyemployee', blank=True)

    def __str__(self):
        return self.name


class AgencyEmployee(CreatedModel):
    """
    AgencyEmployee data model extend agency - employer relation.
    """
    employee = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    agency = models.ForeignKey(Agency, on_delete=models.PROTECT)
