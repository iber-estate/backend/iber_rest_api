import requests

from django.conf import settings


def send_email_message(msg: str, email: str, subject: str = None, sender_name: str = None,
                       mailbox_name: str = None) -> None:
    requests.post(
        f'https://api.eu.mailgun.net/v3/{settings.MAILGUN_DOMAIN_NAME}/messages',
        auth=('api', settings.MAILGUN_API_KEY),
        data={'from': f'{sender_name or settings.MAILGUN_DEFAULT_SENDER_NAME} '
                      f'<{mailbox_name or settings.MAILGUN_DEFAULT_MAILBOX_NAME}@{settings.MAILGUN_DOMAIN_NAME}>',
              'to': [email],
              'subject': subject or settings.EMAIL_DEFAULT_SUBJECT,
              'text': msg})
