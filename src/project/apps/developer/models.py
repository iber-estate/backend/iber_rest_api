from django.db import models
from django.contrib.auth import get_user_model

from phonenumber_field.modelfields import PhoneNumberField

from project.apps.core.models import CreatedModel, CreatedDeletedModel


def upload_to(instance, filename):
    return f'developer/{instance.pk}/{filename}'


class Developer(CreatedDeletedModel):
    """
    Developer data model.
    """
    name = models.CharField(max_length=64, unique=True)
    email = models.EmailField(blank=False, null=True)
    phone = PhoneNumberField(unique=True, blank=True, null=True)
    wa_number = PhoneNumberField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    partner_url = models.URLField(blank=True, null=True)
    private_partner_url = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    avatar = models.ImageField(blank=True, null=True, upload_to=upload_to)
    employees = models.ManyToManyField(
        get_user_model(), related_name='developer', through='developeremployee', blank=True)
    year_of_founding = models.DateField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    creator = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='developers')

    def __str__(self):
        return self.name


class DeveloperEmployee(CreatedModel):
    """
    DeveloperEmployer data model extend developer - user relation.
    """
    employee = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    developer = models.ForeignKey(Developer, on_delete=models.PROTECT)
