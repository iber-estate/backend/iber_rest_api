# Generated by Django 5.0.2 on 2024-04-09 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('property_complex', '0002_alter_propertycomplex_distance_to_sea'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propertycomplex',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, db_index=True),
        ),
    ]
