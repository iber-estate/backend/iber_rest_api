from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth import get_user_model

from project.apps.address.models import Address
from project.apps.core.models import CreatedDeletedModel, CreatedModel
from project.apps.developer.models import Developer


def upload_to(instance, filename):
    return f'property_complex/{instance.property_complex.pk}/{filename}'


class Amenities(models.Model):
    """
    Amenities data model.
    """
    name = models.CharField(max_length=128)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class PropertyComplex(CreatedDeletedModel):
    """
    PropertyComplex data model.
    """

    class ComplexType(models.TextChoices):
        RESIDENTIAL_COMPLEX = 'residential_complex'
        VILLA_COMPLEX = 'villa_complex'
        BUSINESS_COMPLEX = 'business_complex'
        HOTEL = 'hotel'

    name = models.CharField(max_length=64)
    description = models.TextField()
    developer = models.ForeignKey(Developer, on_delete=models.PROTECT, related_name='property_complex')
    address = models.ForeignKey(Address, on_delete=models.PROTECT, related_name='property_complex')
    distance_to_sea = models.PositiveIntegerField(validators=[MaxValueValidator(10000000)], blank=True, null=True)
    complex_type = models.CharField(max_length=64, choices=ComplexType.choices, default=ComplexType.RESIDENTIAL_COMPLEX)
    land_area = models.PositiveSmallIntegerField(validators=[MaxValueValidator(32000)], blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    amenities = models.ManyToManyField(Amenities, related_name='property_complex')
    creator = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='property_complex')
    commission = models.DecimalField(validators=[MinValueValidator(0.1), MaxValueValidator(100.0)],
                                     max_digits=4, decimal_places=1, blank=True, null=True)
    completion_date = models.DateField(blank=True, null=True)
    price_list = models.URLField(blank=True)
    total_properties = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class DocumentLink(models.Model):
    """
    PropertyComplex DocumentLink model.
    """
    property_complex = models.ForeignKey(PropertyComplex, on_delete=models.PROTECT, related_name='documents')
    document_link = models.TextField(blank=True)


class VideoLink(CreatedModel):
    """
    PropertyComplex VideoLink model.
    """
    property_complex = models.ForeignKey(PropertyComplex, on_delete=models.CASCADE, related_name='videos')
    video_link = models.TextField(blank=True)


class Photo(CreatedModel):
    """
    PropertyComplex Photo model.
    """
    property_complex = models.ForeignKey(PropertyComplex, on_delete=models.CASCADE, related_name='photos')
    photo = models.ImageField(upload_to=upload_to)
    order = models.PositiveSmallIntegerField()

    @property
    def photo_url(self):
        return self.photo.url

    class Meta:
        unique_together = ["order", "property_complex"]
