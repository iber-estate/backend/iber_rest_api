from django.contrib import admin
from .models import PropertyComplex


class PropertyComplexAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_filter = ('address__district__city',)


admin.site.register(PropertyComplex, PropertyComplexAdmin)
