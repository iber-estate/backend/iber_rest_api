import redis
from django.conf import settings
from redis import Redis


class RedisManager:
    def __init__(self):
        self._redis = Redis(connection_pool=redis.ConnectionPool.from_url(settings.REDIS_ADDR))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._redis.close()

    def get(self, key):
        return self._redis.get(key)

    def set(self, key, data, ttl=None):
        self._redis.set(key, data)
        self.update_ttl(key, ttl)

    def setnx(self, key, data, ttl=None):
        self._redis.setnx(key, data)
        self.update_ttl(key, ttl)

    def update_ttl(self, key, ttl=None):
        if ttl is not None and self._redis.exists(key):
            self._redis.expire(key, ttl)

    def keys(self, pattern):
        return self._redis.keys(pattern)

    def delete(self, key):
        return self._redis.delete(key)

    def rpush(self, key, data, ttl=None):
        self._redis.rpush(key, data)
        self.update_ttl(key, ttl)

    def rpop(self, key):
        return self._redis.rpop(key)

    def llen(self, key):
        return self._redis.llen(key)
