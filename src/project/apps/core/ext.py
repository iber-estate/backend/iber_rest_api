from django.utils import timezone

from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class DefaultPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def paginate_queryset(self, queryset, request, view=None):
        if request.query_params.get('get_all', False):
            return None

        return super(DefaultPagination, self).paginate_queryset(queryset, request, view=view)

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })


class CeleryResultSerializer(serializers.Serializer):
    result_id = serializers.UUIDField(
        help_text="Celery task id(for example b4339385-7e83-4d99-a485-92bd3f00f1b9)"
    )


YEAR_CHOICES = [(r, r) for r in range(2000, timezone.now().year + 5)]
