import logging

from project import celery_app


@celery_app.task()
def celery_health_task(**kwargs):
    logging.info('requested {}'.format(kwargs), extra={'trace': 'N/A'})
    return "ok"
