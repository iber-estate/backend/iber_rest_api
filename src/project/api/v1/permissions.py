from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission

User = get_user_model()


class AdminPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.role == User.Role.ADMINISTRATOR


class CustomerPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.role == User.Role.CUSTOMER


class FreelancerPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.role == User.Role.FREELANCER


class DeveloperPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.role == User.Role.DEVELOPER


class RealtorPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.role == User.Role.REALTOR
