from rest_framework import serializers

from project.apps.agency.models import Agency


class AgencySerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ['deleted_at']
        model = Agency


class AgencyListSerializer(serializers.ModelSerializer):
    employees = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        exclude = ['deleted_at']
        model = Agency
