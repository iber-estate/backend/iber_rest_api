from django.db.utils import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from project.api.v1.serializers.real_property import RealPropertyShortSerializer
from project.api.v1.serializers.user import UserFullSerializer
from project.apps.listing.models import Listing


class ListingListSerializer(serializers.ModelSerializer):
    properties = RealPropertyShortSerializer(many=True)
    user = UserFullSerializer()

    class Meta:
        exclude = ['deleted_at']
        model = Listing


class ListingSerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ['user', 'deleted_at']
        model = Listing

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        try:
            listing = super().create(validated_data)
        except IntegrityError:
            raise ValidationError({"name": [f'A unique value is required.']})
        return listing


class PropertyToListingSerializer(serializers.Serializer):
    property_id = serializers.IntegerField(required=True)
