from django.conf import settings

from rest_framework import serializers
from sorl.thumbnail import get_thumbnail
from drf_yasg.utils import swagger_serializer_method

from project.api.v1.exceptions import ValidationAPIError
from project.api.v1.serializers import FilterDeleteListSerializer
from project.api.v1.serializers.address import AddressListSerializer
from project.api.v1.serializers.property_complex import PropertyComplexListSerializer
from project.api.v1.serializers.user import UserFullSerializer
from project.apps.real_property.models import (
    RealProperty,
    Amenities,
    OwnershipType,
    Tag,
    Photo,
    Document,
    Video,
    SavedProperty,
)


class RealPropertyAmenitiesListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        fields = '__all__'
        model = Amenities


class TagsListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        fields = '__all__'
        model = Tag


class OwnershipTypesListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    name = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = OwnershipType

    @staticmethod
    def get_name(obj):
        return f'({obj.country.name}) {obj.name}'


class PhotoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        exclude = ['order', 'real_property', 'deleted_at']


class PhotoSerializer(serializers.ModelSerializer):
    thumbnail = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Photo
        exclude = ['deleted_at']
        list_serializer_class = FilterDeleteListSerializer

    def get_thumbnail(self, instance):
        thumbnail = get_thumbnail(
            instance.photo, settings.THUMBNAIL_DEFAULT_SIZE, crop='center', quality=settings.THUMBNAIL_DEFAULT_QUALITY)
        return f"{settings.SWAGGER_SCHEMA}://{settings.DOMAIN_NAME}{thumbnail.url}"


class PhotoOrderSerializer(serializers.Serializer):
    sorted_photo_list = serializers.ListField(child=serializers.IntegerField(), required=True)


class VideoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ['external_url', 'video']


class DocumentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ['external_url', 'description', 'file']


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        exclude = ['deleted_at']
        list_serializer_class = FilterDeleteListSerializer

    def create(self, validated_data):
        if not validated_data.get('video') and not validated_data.get('external_url'):
            raise ValidationAPIError(detail='external_url or video file is required')
        return super().create(validated_data)


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        exclude = ['deleted_at']
        list_serializer_class = FilterDeleteListSerializer

    def create(self, validated_data):
        if not validated_data.get('video') and not validated_data.get('external_url'):
            raise ValidationAPIError(detail='external_url or document file is required')
        return super().create(validated_data)


class RealPropertyListSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(many=True)
    videos = VideoSerializer(many=True)
    address = AddressListSerializer()
    property_complex = PropertyComplexListSerializer()
    amenities = RealPropertyAmenitiesListSerializer(many=True)
    tags = TagsListSerializer(many=True)
    documents = DocumentSerializer(many=True)
    ownership_type = OwnershipTypesListSerializer()
    creator = UserFullSerializer()
    is_saved = serializers.SerializerMethodField()

    class Meta:
        exclude = ['deleted_at']
        model = RealProperty

    @swagger_serializer_method(serializer_or_field=serializers.BooleanField())
    def get_is_saved(self, instance):
        return True if self.context['request'].user.is_authenticated and SavedProperty.objects.filter(
            user=self.context['request'].user, real_property=instance).first() else False


class RealPropertyShortSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(many=True)
    videos = VideoSerializer(many=True)
    amenities = RealPropertyAmenitiesListSerializer(many=True)
    tags = TagsListSerializer(many=True)
    documents = DocumentSerializer(many=True)
    address = AddressListSerializer()

    class Meta:
        exclude = ['deleted_at']
        model = RealProperty


class RealPropertyCreateUpdateSerializer(serializers.ModelSerializer):
    # Representing arrays of files is not doable with OpenAPI 2.0 restrictions. Will render the files field as read-only
    photos = PhotoSerializer(many=True, read_only=True)
    documents = DocumentSerializer(many=True, read_only=True)
    videos = VideoSerializer(many=True, read_only=True)
    description = serializers.CharField(max_length=10000)

    class Meta:
        exclude = ['creator', 'deleted_at']
        model = RealProperty

    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        return super().create(validated_data)


class SavedPropertyListSerializer(serializers.ModelSerializer):
    real_property = RealPropertyListSerializer(read_only=True)

    class Meta:
        fields = ['real_property']
        model = SavedProperty


class SavedPropertyCrateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['real_property']
        model = SavedProperty

    def create(self, validated_data):
        user = self.context['request'].user
        saved_property = SavedProperty.objects.filter(
                real_property=validated_data['real_property'], user=self.context['request'].user).first()
        if saved_property:
            return saved_property
        validated_data['user'] = user
        user.new_notification = True
        user.save()
        return super().create(validated_data)
