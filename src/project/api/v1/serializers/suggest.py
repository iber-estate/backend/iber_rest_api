from django.db.models import Q

from rest_framework import serializers

from project.apps.address.models import Address, Country
from project.apps.developer.models import Developer
from project.apps.real_property.models import RealProperty


class SuggestSerializer(serializers.Serializer):
    input = serializers.CharField(required=True, write_only=True)
    countries = serializers.SerializerMethodField(read_only=True)
    addresses = serializers.SerializerMethodField(read_only=True)
    # developers = serializers.SerializerMethodField(read_only=True)
    # real_property = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_countries(obj):
        name_q = Q()
        for part in obj.get('input', '').split(" "):
            name_q.add(Q(name__icontains=part), Q.AND)
        return [{'id': c[0], 'name': c[1]}
                for c in Country.objects.non_deleted().filter(name_q)[:3].values_list('pk', 'name')]

    @staticmethod
    def get_addresses(obj):
        q = Q()
        for part in obj.get('input', '').split(" "):
            q.add(Q(line_1__icontains=part), Q.AND)
        return [{'id': a[0], 'line_1': a[1]}
                for a in Address.objects.non_deleted().filter(q)[:3].values_list('pk', 'line_1')]

    @staticmethod
    def get_developers(obj):
        name_q = Q()
        for part in obj.get('input', '').split(" "):
            name_q.add(Q(name__icontains=part), Q.AND)
        return [{'id': d[0], 'name': d[1]}
                for d in Developer.objects.non_deleted().filter(name_q)[:3].values_list('pk', 'name')]

    @staticmethod
    def get_real_property(obj):
        name_q = Q()
        for part in obj.get('input', '').split(" "):
            name_q.add(Q(name__icontains=part), Q.AND)
        return [{'id': rp[0], 'name': rp[1]}
                for rp in RealProperty.objects.non_deleted().filter(name_q)[:3].values_list('pk', 'name')]
