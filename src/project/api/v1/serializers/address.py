import json
import logging

from django.contrib.gis.geos import GEOSGeometry, Polygon

from geopy.geocoders import Nominatim
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from project.apps.address.models import Country, City, Address, District


# ------------------------------------------- Country model serializers --------------------------------------- #
class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ('created_at', 'deleted_at',)
        model = Country


class CountryShortSerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ('created_at', 'deleted_at',)
        model = Country


class CountryListSerializer(serializers.ModelSerializer):
    cities = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')

    class Meta:
        exclude = ('created_at', 'deleted_at',)
        model = Country


# ------------------------------------------- City model serializers ----------------------------------------- #
class CitySerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ('deleted_at',)
        model = City


class CityShortSerializer(serializers.ModelSerializer):
    country = serializers.CharField(source="country.name")

    class Meta:
        exclude = ('created_at', 'deleted_at')
        model = City


class CityListSerializer(serializers.ModelSerializer):
    country = CountryShortSerializer()

    class Meta:
        fields = ('id', 'name', 'country', 'created_at')
        model = City


# ------------------------------------------- District model serializers ----------------------------------------- #
class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ('deleted_at',)
        model = District


class DistrictShortSerializer(serializers.ModelSerializer):
    city = serializers.CharField(source="city.name")

    class Meta:
        exclude = ('created_at', 'deleted_at')
        model = District


class DistrictListSerializer(serializers.ModelSerializer):
    city = CityListSerializer()

    class Meta:
        exclude = ('deleted_at',)
        model = District


# ------------------------------------------- Address model serializers ----------------------------------------- #
class AddressCreateUpdateSerializer(serializers.ModelSerializer):
    line_1 = serializers.CharField(required=False, read_only=True)
    line_2 = serializers.CharField(required=False, read_only=True)
    district = serializers.CharField(required=False, read_only=True)
    geo_point = serializers.CharField(required=False, read_only=True)

    class Meta:
        exclude = ('deleted_at',)
        model = Address

    @staticmethod
    def _get_location_by_coordinates(lat: str, lng: str) -> dict:
        logging.info('_get_location_by_coordinates: started')
        try:
            geolocator = Nominatim(user_agent="iber_app")
            location = geolocator.reverse(f"{lat}, {lng}", language="en").raw
        except Exception as e:
            logging.error(f'AddressCreateUpdateSerializer: geolocator error - {e}')
            raise ValidationError({"address": [f'geolocator error']})
        logging.info('_get_location_by_coordinates: ended')
        return location

    def get_unique_together_validators(self):
        """Overriding method to disable unique together checks"""
        return []

    def create(self, validated_data):
        logging.info('AddressCreateUpdateSerializer:create: started')

        # check existed address
        address = Address.objects.filter(lat=validated_data['lat'], lng=validated_data['lng']).first()
        if address:
            logging.info('AddressCreateUpdateSerializer:create: address already exists. ended')
            return address

        # get location data and check location address and county
        location = self._get_location_by_coordinates(validated_data['lat'], validated_data['lng'])
        location_address = location.get('address')
        if not location_address:
            logging.error(f'AddressCreateUpdateSerializer:create: incorrect parsed address. ended')
            raise ValidationError({"location": [f'can not find any address by this coordinates']})
        country = location_address.get('country')
        if not country or not Country.objects.non_deleted().filter(name=country):
            logging.error(f'AddressCreateUpdateSerializer:create: incorrect country. ended')
            raise ValidationError({"country": [f'can not find allowed country by this coordinates']})
        location_geo_poit = GEOSGeometry(f"POINT({validated_data['lng']} {validated_data['lat']})")

        # try to find location district
        districts = District.objects.filter(city__country__name=country, geo_polygon__isnull=False)
        for district in districts:
            if district.geo_polygon and Polygon(district.geo_polygon['coordinates'][0][0]).contains(location_geo_poit):
                validated_data['district'] = district
        if not validated_data.get('district'):
            logging.warning(f'AddressCreateUpdateSerializer:create: can not find any district.')

        # collect additional data
        validated_data['geo_point'] = json.loads(location_geo_poit.geojson)
        if location_address.get('postcode'):
            validated_data['postcode'] = location_address['postcode']
        if location.get('display_name'):
            validated_data['line_1'] = location['display_name']

        logging.info('AddressCreateUpdateSerializer:create: ended')
        return super().create(validated_data)


class AddressListSerializer(serializers.ModelSerializer):
    district = DistrictListSerializer()
    line_1 = serializers.SerializerMethodField()

    class Meta:
        exclude = ('deleted_at',)
        model = Address

    @staticmethod
    def get_line_1(instance):
        return instance.line_1 if not instance.district else \
            f'{instance.district.city.country.name}, {instance.district.city.name}, {instance.district.name}'
