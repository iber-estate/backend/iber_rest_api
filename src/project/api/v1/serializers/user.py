import random
import string

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password, check_password
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.tokens import RefreshToken

from project.apps.user.models import OneTimePassword
from project.apps.user.tasks import send_auth_email_task, send_system_notification_email_task

User = get_user_model()


def _get_user_by_email(email: str) -> User:
    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        raise ValidationError({"email": ['user not registered']})
    return user


class UserFullSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id', 'email', 'name', 'phone', 'description', 'wa_number', 'role', 'new_notification', 'date_joined']
        read_only_fields = fields
        model = User


class UserShortSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id', 'email', 'name', 'phone']
        read_only_fields = fields
        model = User


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'name', 'phone', 'description', 'wa_number', 'role', 'date_joined']
        read_only_fields = ['date_joined']

    @staticmethod
    def validate_role(attr):
        if attr in [User.Role.ADMINISTRATOR, User.Role.DEVELOPER]:
            raise ValidationError(f'You can not create user with role.')
        return attr

    def create(self, validated_data):
        if not validated_data.get('role'):
            validated_data['role'] = User.Role.REALTOR
        user = super().create(validated_data)
        serializer = OneTimePasswordCreateSerializer(data={'email': user.email})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        send_system_notification_email_task.delay(
            f'New user was registered. Email: {user.email} | Name: {user.name}')
        return user


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['name', 'phone', 'description', 'wa_number', 'role']

    def validate_role(self, attr):
        if self.context['request'].user.role != User.Role.ADMINISTRATOR:
            raise ValidationError(f'You can not update user role.')
        return attr


class UserAccessTokenSerializer(serializers.Serializer):
    refresh = serializers.CharField()
    access = serializers.CharField()


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    @staticmethod
    def _get_tokens_for_user(user: User):
        refresh = RefreshToken.for_user(user)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    def get_access(self, validated_data):
        otp = OneTimePassword.objects.filter(user=_get_user_by_email(validated_data['email'])).last()
        if not otp or not check_password(validated_data['password'], otp.password) or not otp.is_valid:
            raise ValidationError({"password": ['Invalid OTP']})
        return self._get_tokens_for_user(user=otp.user)


class OneTimePasswordCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(required=False)

    @staticmethod
    def _create_and_send_otp(email: str) -> str:
        raw_password = ''.join(random.choices(string.ascii_lowercase + string.digits, k=settings.OTP_LENGTH))
        send_auth_email_task.delay(email=email, password=raw_password)
        return make_password(raw_password)

    class Meta:
        model = OneTimePassword
        fields = ['email', 'password']
        read_only_fields = fields

    def create(self, validated_data):
        email = validated_data.pop('email')
        validated_data['user'] = _get_user_by_email(email)
        validated_data['password'] = self._create_and_send_otp(email)
        return super().create(validated_data)
