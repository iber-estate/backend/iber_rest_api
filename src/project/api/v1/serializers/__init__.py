from rest_framework import serializers


class BadRequestResponseSerializer(serializers.Serializer):
    errors = serializers.DictField()


class CommonErrorResponseSerializer(serializers.Serializer):
    detail = serializers.CharField()
    status_code = serializers.IntegerField()
    error = serializers.CharField()


class FilterDeleteListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        return super(FilterDeleteListSerializer, self).to_representation(data.non_deleted())
