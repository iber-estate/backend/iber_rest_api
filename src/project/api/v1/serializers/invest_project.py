from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import serializers
from sorl.thumbnail import get_thumbnail

from project.api.v1.serializers.user import UserShortSerializer
from project.api.v1.serializers.address import AddressListSerializer
from project.apps.invest_project.models import (
    InvestProject,
    DocumentLink,
    Photo,
    InvestCategory,
    VideoLink,PresentationLink
)

User = get_user_model()


class InvestCategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = InvestCategory


class InvestDocumentLinkSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = DocumentLink


class InvestDocumentLinkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['document_link']
        model = DocumentLink


class InvestProjectPhotoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        exclude = ['order', 'invest_project']


class InvestProjectPhotoSerializer(serializers.ModelSerializer):
    thumbnail = serializers.SerializerMethodField(read_only=True)

    class Meta:
        fields = '__all__'
        model = Photo

    def get_thumbnail(self, instance):
        thumbnail = get_thumbnail(
            instance.photo, settings.THUMBNAIL_DEFAULT_SIZE, crop='center', quality=settings.THUMBNAIL_DEFAULT_QUALITY)
        return f"{settings.SWAGGER_SCHEMA}://{settings.DOMAIN_NAME}{thumbnail.url}"


class InvestProjectPhotoOrderSerializer(serializers.Serializer):
    sorted_photo_list = serializers.ListField(child=serializers.IntegerField(), required=True)


class InvestProjectVideoLinkSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = VideoLink


class InvestProjectVideoLinkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['video_link']
        model = VideoLink


class InvestProjectPresentationLinkSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = PresentationLink


class InvestProjectPresentationLinkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['presentation_link']
        model = PresentationLink


class InvestProjectCreateUpdateSerializer(serializers.ModelSerializer):
    documents = InvestDocumentLinkSerializer(many=True, read_only=True)
    photos = InvestProjectPhotoCreateSerializer(many=True, read_only=True)
    email = serializers.EmailField(required=True, write_only=True)
    videos = InvestProjectVideoLinkSerializer(many=True, read_only=True)
    presentations = InvestProjectPresentationLinkSerializer(many=True, read_only=True)
    draft_address = serializers.CharField(required=True)
    user_full_name = serializers.CharField(required=True)
    access = serializers.CharField(read_only=True)

    class Meta:
        fields = '__all__'
        read_only_fields = ['user', 'access']
        model = InvestProject

    def create(self, validated_data):
        email = validated_data.pop('email')
        user = User.objects.filter(email=email).first()
        if not user:
            user = User.objects.create(
                email=email, name=validated_data['user_full_name'], phone=validated_data['phone'])
        validated_data['user'] = user
        return super().create(validated_data)


class InvestProjectListSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    financial_plan = serializers.CharField(required=False)
    business_plan = serializers.CharField(required=False)
    invest_amount = serializers.IntegerField(required=False)
    expected_profit = serializers.IntegerField(required=False)
    payback_period = serializers.IntegerField(required=False)
    category = InvestCategorySerializer(read_only=True)
    address = AddressListSerializer(read_only=True)
    user = UserShortSerializer(read_only=True)
    documents = InvestDocumentLinkSerializer(many=True, read_only=True)
    photos = InvestProjectPhotoCreateSerializer(many=True, read_only=True)
    videos = InvestProjectVideoLinkSerializer(many=True, read_only=True)
    presentations = InvestProjectPresentationLinkSerializer(many=True, read_only=True)

    class Meta:
        exclude = ['deleted_at']
        model = InvestProject
