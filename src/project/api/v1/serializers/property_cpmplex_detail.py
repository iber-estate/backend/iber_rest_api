from rest_framework import serializers

from project.api.v1.serializers.address import AddressListSerializer
from project.api.v1.serializers.developer import DeveloperListSerializer
from project.api.v1.serializers.property_complex import PropertyComplexAmenitiesListSerializer, DocumentLinkSerializer, \
    PropertyComplexPhotoSerializer, PropertyComplexVideoLinkSerializer
from project.api.v1.serializers.real_property import RealPropertyShortSerializer
from project.apps.property_complex.models import PropertyComplex


class PropertyComplexDetailSerializer(serializers.ModelSerializer):
    address = AddressListSerializer()
    developer = DeveloperListSerializer()
    amenities = PropertyComplexAmenitiesListSerializer(many=True)
    numbers_of_properties = serializers.SerializerMethodField()
    min_price = serializers.SerializerMethodField()
    max_price = serializers.SerializerMethodField()
    documents = DocumentLinkSerializer(many=True)
    photos = PropertyComplexPhotoSerializer(many=True)
    videos = PropertyComplexVideoLinkSerializer(many=True)
    property = RealPropertyShortSerializer(source='real_property', many=True)

    class Meta:
        exclude = ['deleted_at']
        model = PropertyComplex

    @staticmethod
    def get_numbers_of_properties(instance):
        return instance.real_property.non_deleted().count()

    @staticmethod
    def get_min_price(instance):
        real_property = instance.real_property.non_deleted().order_by('price')
        return real_property[0].price if real_property else 0

    @staticmethod
    def get_max_price(instance):
        real_property = instance.real_property.non_deleted().order_by('-price')
        return real_property[0].price if real_property else 0
