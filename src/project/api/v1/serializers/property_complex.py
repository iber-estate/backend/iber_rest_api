from django.conf import settings

from rest_framework import serializers
from sorl.thumbnail import get_thumbnail

from project.api.v1.serializers.address import AddressListSerializer
from project.api.v1.serializers.developer import DeveloperListSerializer
from project.apps.property_complex.models import PropertyComplex, Amenities, DocumentLink, VideoLink, Photo


class PropertyComplexAmenitiesListSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        fields = '__all__'
        model = Amenities


class DocumentLinkSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = DocumentLink


class DocumentLinkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentLink
        fields = ['document_link']


class PropertyComplexPhotoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        exclude = ['order', 'property_complex']


class PropertyComplexPhotoSerializer(serializers.ModelSerializer):
    thumbnail = serializers.SerializerMethodField(read_only=True)

    class Meta:
        fields = '__all__'
        model = Photo

    def get_thumbnail(self, instance):
        thumbnail = get_thumbnail(
            instance.photo, settings.THUMBNAIL_DEFAULT_SIZE, crop='center', quality=settings.THUMBNAIL_DEFAULT_QUALITY)
        return f"{settings.SWAGGER_SCHEMA}://{settings.DOMAIN_NAME}{thumbnail.url}"


class PropertyComplexPhotoOrderSerializer(serializers.Serializer):
    sorted_photo_list = serializers.ListField(child=serializers.IntegerField(), required=True)


class PropertyComplexVideoLinkSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = VideoLink


class PropertyComplexVideoLinkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoLink
        fields = ['video_link']


class PropertyComplexCreateUpdateSerializer(serializers.ModelSerializer):
    documents = DocumentLinkSerializer(many=True, read_only=True)
    videos = PropertyComplexVideoLinkSerializer(many=True, read_only=True)
    photos = PropertyComplexPhotoCreateSerializer(many=True, read_only=True)

    class Meta:
        fields = ['id', 'documents', 'videos', 'photos',  'name', 'description', 'distance_to_sea',
                  'complex_type', 'land_area', 'developer', 'address', 'amenities', 'total_properties', 'price_list']
        model = PropertyComplex

    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        instance = super().create(validated_data)
        return instance


class PropertyComplexListSerializer(serializers.ModelSerializer):
    address = AddressListSerializer()
    developer = DeveloperListSerializer()
    amenities = PropertyComplexAmenitiesListSerializer(many=True)
    documents = DocumentLinkSerializer(many=True)
    photos = PropertyComplexPhotoSerializer(many=True)
    videos = PropertyComplexVideoLinkSerializer(many=True)
    total_configurations_options = serializers.SerializerMethodField()
    min_price = serializers.SerializerMethodField()
    max_price = serializers.SerializerMethodField()

    class Meta:
        fields = ['id', 'documents', 'videos', 'photos', 'name', 'description', 'distance_to_sea',
                  'complex_type', 'land_area', 'developer', 'address', 'amenities', 'min_price', 'max_price',
                  'total_properties', 'total_configurations_options', 'price_list']
        model = PropertyComplex

    @staticmethod
    def get_total_configurations_options(instance):
        return instance.real_property.non_deleted().count()

    @staticmethod
    def get_min_price(instance):
        real_property = instance.real_property.non_deleted().order_by('price')
        return real_property[0].price if real_property else 0

    @staticmethod
    def get_max_price(instance):
        real_property = instance.real_property.non_deleted().order_by('-price')
        return real_property[0].price if real_property else 0
