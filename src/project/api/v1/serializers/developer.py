from rest_framework import serializers

from project.apps.developer.models import Developer
from project.apps.real_property.models import RealProperty


class DeveloperCreateUpdateSerializer(serializers.ModelSerializer):
    description = serializers.CharField(max_length=10000)

    class Meta:
        exclude = ['creator', 'avatar', 'deleted_at']
        model = Developer

    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        return super().create(validated_data)


class DeveloperListSerializer(serializers.ModelSerializer):
    employees = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')
    numbers_of_properties = serializers.SerializerMethodField()

    class Meta:
        exclude = ['deleted_at']
        model = Developer

    def get_numbers_of_properties(self, instance):
        return RealProperty.objects.non_deleted().filter(property_complex__developer=instance).count()


class DeveloperAvatarSerializer(serializers.Serializer):
    avatar = serializers.ImageField()

    def update(self, instance, validated_data):
        instance.avatar = validated_data['avatar']
        instance.save()
        return instance
