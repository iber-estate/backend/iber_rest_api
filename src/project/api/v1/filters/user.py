from django_filters.rest_framework import FilterSet
from django.contrib.auth import get_user_model

User = get_user_model()


class UserFilterSet(FilterSet):
    """
    User filter
    """

    class Meta:
        model = User
        fields = ['name', 'is_active', 'role', 'is_active']
