from django_filters.rest_framework import FilterSet, CharFilter

from project.apps.developer.models import Developer


class DeveloperFilterSet(FilterSet):
    """
    Developer filter
    """

    address = CharFilter(
        field_name='property_complex__address__line_1',
        lookup_expr='icontains',
        label='Filter by address line 1. For example: address=Indonesia'
    )

    class Meta:
        model = Developer
        fields = ['name', 'address']
