from django_filters.rest_framework import FilterSet, CharFilter

from project.apps.address.models import Country, City, District, Address


class CountryFilterSet(FilterSet):
    """Country filter"""

    name = CharFilter(
        field_name='name',
        lookup_expr='icontains',
        label='Filter by name. For example: name=Indonesia'
    )

    class Meta:
        model = Country
        fields = ['name']


class CityFilterSet(FilterSet):
    """City filter"""

    name = CharFilter(
        field_name='name',
        lookup_expr='icontains',
        label='Filter by name. For example: name=Denpasar'
    )

    class Meta:
        model = City
        fields = ['name', 'country']


class DistrictFilterSet(FilterSet):
    """District filter"""

    name = CharFilter(
        field_name='name',
        lookup_expr='icontains',
        label='Filter by name. For example: name=d1'
    )

    class Meta:
        model = District
        fields = ['name', 'city']


class AddressFilterSet(FilterSet):
    """Address filter"""

    line1 = CharFilter(
        field_name='line1',
        lookup_expr='icontains',
        label='Filter by line1.'
    )

    class Meta:
        model = Address
        fields = ['district', 'postcode', 'line1']
