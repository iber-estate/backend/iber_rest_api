from django.db.models import Q
from django_filters.rest_framework import FilterSet, CharFilter, NumberFilter

from project.apps.invest_project.models import InvestProject
from project.apps.address.models import Country


class InvestProjectFilterSet(FilterSet):
    """
    Invest Project filter
    """
    invest_amount_min = NumberFilter(
        label='Filter by min invest_amount.', field_name='invest_amount', method='get_invest_amount_min')
    invest_amount_max = NumberFilter(
        label='Filter by max invest_amount.', field_name='invest_amount', method='get_invest_amount_max')
    country = CharFilter(
        field_name='address',
        method='get_country',
        label='Filter by country id. You can pass multiple values separated by commas. For example: country=1,2'
    )
    address = CharFilter(
        field_name='address__line_1',
        lookup_expr='icontains',
        label='Filter by address line 1. For example: address=Indonesia'
    )

    class Meta:
        model = InvestProject
        fields = ['address', 'country', 'invest_amount_min', 'invest_amount_max']

    @staticmethod
    def _parse_str_to_list(values: str) -> list:
        values_list = []
        for value in values.split(','):
            try:
                values_list.append(int(value))
            except ValueError:
                continue
        return values_list

    @staticmethod
    def get_invest_amount_min(queryset, name, value):
        return queryset.filter(invest_amount__gte=value)

    @staticmethod
    def get_invest_amount_max(queryset, name, value):
        return queryset.filter(invest_amount__lte=value)

    def get_country(self, queryset, name, value):
        country_list = self._parse_str_to_list(value)
        country_list = Country.objects.filter(pk__in=country_list).values_list('name', flat=True)
        if country_list:
            q = Q()
            for country in country_list:
                q.add(Q(address__line_1__icontains=country), Q.OR)
            queryset = queryset.filter(q)
        return queryset
