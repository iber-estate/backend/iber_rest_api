from django_filters.rest_framework import FilterSet

from project.apps.agency.models import Agency


class AgencyFilterSet(FilterSet):
    """Agency filter"""

    class Meta:
        model = Agency
        fields = ['name']
