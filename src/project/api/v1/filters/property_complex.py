from django_filters.rest_framework import FilterSet

from project.apps.property_complex.models import PropertyComplex


class PropertyComplexFilterSet(FilterSet):
    """
    RealProperty filter
    """

    class Meta:
        model = PropertyComplex
        fields = ['name', 'developer']

