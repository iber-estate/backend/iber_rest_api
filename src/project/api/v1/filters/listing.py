from django_filters.rest_framework import FilterSet

from project.apps.listing.models import Listing


class ListingFilterSet(FilterSet):
    """Listing filter"""

    class Meta:
        model = Listing
        fields = ['name']
