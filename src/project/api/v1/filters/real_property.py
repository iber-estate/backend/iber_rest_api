from django.db.models import Q
from django_filters.rest_framework import FilterSet, NumberFilter, CharFilter, BooleanFilter

from project.apps.address.models import Country
from project.apps.real_property.models import RealProperty, SavedProperty


class RealPropertyFilterSet(FilterSet):
    """RealProperty filter"""

    price_min = NumberFilter(label='Filter by min price.', field_name='price', method='get_price_min')
    price_max = NumberFilter(label='Filter by max price.', field_name='price', method='get_price_max')
    bedrooms = CharFilter(
        field_name='bedrooms',
        method='get_bedrooms',
        label='Filter by bedrooms number. You can pass multiple values separated by commas. '
              'For example: bedrooms=1,2')
    bathrooms = CharFilter(
        field_name='bathrooms',
        method='get_bathrooms',
        label='Filter by bathrooms number. You can pass multiple values separated by commas. '
               'For example: bathrooms=1,2')
    country = CharFilter(
        field_name='address',
        method='get_country',
        label='Filter by country id. You can pass multiple values separated by commas. For example: country=1,2'
    )
    address = CharFilter(
        field_name='address__line_1',
        lookup_expr='icontains',
        label='Filter by line1.'
    )
    property_type = CharFilter(
        field_name='property_type',
        method='get_property_type',
        label='Filter by property type. You can pass multiple values separated by commas. '
              'For example: property_type=villa,apartment')
    is_saved = BooleanFilter(label='Filter by saved mark.', method='get_is_saved')

    class Meta:
        model = RealProperty
        fields = ['price_min', 'address', 'price_max', 'bedrooms', 'bathrooms', 'country', 'property_type',
                  'is_saved', 'status']

    @staticmethod
    def _parse_str_to_list(values: str) -> list:
        values_list = []
        for value in values.split(','):
            try:
                values_list.append(int(value))
            except ValueError:
                continue
        return values_list

    @staticmethod
    def get_price_min(queryset, name, value):
        return queryset.filter(price__gte=value)

    @staticmethod
    def get_price_max(queryset, name, value):
        return queryset.filter(price__lte=value)

    def get_bedrooms(self, queryset, name, value):
        bedrooms_list = self._parse_str_to_list(value)
        if bedrooms_list:
            return queryset.filter(bedrooms__in=bedrooms_list)
        return queryset

    def get_bathrooms(self, queryset, name, value):
        bathrooms_list = self._parse_str_to_list(value)
        if bathrooms_list:
            return queryset.filter(bathrooms__in=bathrooms_list)
        return queryset

    def get_country(self, queryset, name, value):
        country_list = self._parse_str_to_list(value)
        country_list = Country.objects.filter(pk__in=country_list).values_list('name', flat=True)
        if country_list:
            q = Q()
            for country in country_list:
                q.add(Q(address__line_1__icontains=country), Q.OR)
            queryset = queryset.filter(q)
        return queryset

    @staticmethod
    def get_property_type(queryset, name, value):
        property_type_list = []
        for v in value.split(','):
            if v in [t[0] for t in RealProperty.PropertyType.choices]:
                property_type_list.append(v)
        if property_type_list:
            return queryset.filter(property_type__in=property_type_list)
        return queryset

    def get_is_saved(self, queryset, name, value):
        if self.request.user.is_authenticated:
            user_saved_property = SavedProperty.objects.filter(user=self.request.user).values_list('pk', flat=True)
            return queryset.filter(pk__in=user_saved_property)
        return queryset
