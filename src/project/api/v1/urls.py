from django.urls import include, path
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, routers
from rest_framework_simplejwt.views import TokenRefreshView

from project.api.v1.views.address import AddressViewSet, CityViewSet, DistrictViewSet, CountryViewSet
from project.api.v1.views.agency import AgencyViewSet
from project.api.v1.views.developer import DeveloperViewSet
from project.api.v1.views.health import HealthCheckView
from project.api.v1.views.invest_project import InvestProjectViewSet
from project.api.v1.views.listing import ListingViewSet
from project.api.v1.views.property_complex import PropertyComplexViewSet
from project.api.v1.views.real_property import RealPropertyViewSet, SavedPropertyViewSet
from project.api.v1.views.statistic import StatisticView
from project.api.v1.views.suggest import SuggestView
from project.api.v1.views.user import UserViewSet
from project.api.v1.views.celery import CeleryResultView


router = routers.DefaultRouter()
router.register(r'country', CountryViewSet, basename='country')
router.register(r'city', CityViewSet, basename='city')
router.register(r'district', DistrictViewSet, basename='district')
router.register(r'address', AddressViewSet, basename='address')
router.register(r'agency', AgencyViewSet, basename='agency')
router.register(r'complex', PropertyComplexViewSet, basename='complex')
router.register(r'developer', DeveloperViewSet, basename='developer')
router.register(r'saved', SavedPropertyViewSet, basename='saved')
router.register(r'listing', ListingViewSet, basename='listing')
router.register(r'property', RealPropertyViewSet, basename='property')
router.register(r'project', InvestProjectViewSet, basename='project')
router.register(r'user', UserViewSet, basename='user')

description = """Enter the token with the Bearer prefix
Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjg2NDA4ODEzLCJpYXQiOjE2ODYzMjI0
The token is remembered during authorization - if it has expired, you need to log out and get a new one.
"""

schema_view = get_schema_view(
    openapi.Info(
        title="Iber API",
        default_version='v1',
        description=description,
        contact=openapi.Contact(email=""),
        license=openapi.License(name="Iber license"),
    ),
    public=True,
    url=f'{settings.SWAGGER_SCHEMA}://{settings.DOMAIN_NAME}{settings.PROJECT_URI}/',
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('swagger(<str:format>.json|.yaml)', schema_view.without_ui(), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger'), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc'), name='schema-redoc'),
    path('celery/result/<pk>/', CeleryResultView.as_view()),
    path('', include((router.urls, 'api-root')), name='api-root'),
    path('user/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('health/', HealthCheckView.as_view(), name='health-check'),
    path('statistic/', StatisticView.as_view(), name='statistic'),
    path('suggest/', SuggestView.as_view(), name='suggest')
]
