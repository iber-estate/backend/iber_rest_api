import logging

from django.db.models import Sum
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from project.apps.property_complex.models import PropertyComplex


class StatisticView(APIView):
    permission_classes = [AllowAny]

    def get(self, _):
        logging.info("get-statistic: requested")
        return Response(
            data={'total_properties': PropertyComplex.objects.non_deleted().aggregate(Sum('total_properties')).get('total_properties__sum')},
            status=status.HTTP_200_OK)
