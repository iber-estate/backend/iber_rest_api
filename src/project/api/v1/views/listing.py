from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from project.api.v1.filters import ListingFilterSet
from project.api.v1.permissions import AdminPermissions, RealtorPermissions
from project.api.v1.serializers.listing import ListingListSerializer, ListingSerializer, PropertyToListingSerializer
from project.api.v1.viewsets import ExtendedModelViewSet
from project.apps.listing.models import Listing
from project.apps.real_property.models import RealProperty


class ListingViewSet(ExtendedModelViewSet):
    """
    View for working with listing data.

    create:
    Create new listing.

    retrieve:
    Return the given listing.

    list:
    List all listings.

    update:
    Update listing data.

    partial_update:
    Partial update listing data.

    delete:
    Mark listing as deleted.
    """

    queryset = Listing.objects.non_deleted()
    serializer_class = ListingSerializer
    serializer_class_map = {
        'list': ListingListSerializer,
        'retrieve': ListingListSerializer
    }
    permission_map = {
        'create': (AdminPermissions | RealtorPermissions),
        'update': (AdminPermissions | RealtorPermissions),
        'partial_update': (AdminPermissions | RealtorPermissions),
        'list': (AdminPermissions | RealtorPermissions),
        'retrieve': AllowAny,
        'destroy': (AdminPermissions | RealtorPermissions),
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = ListingFilterSet
    ordering_fields = ['created_at']
    property_pk = openapi.Parameter(
        'property_id', openapi.IN_PATH, description="property id", type=openapi.TYPE_INTEGER
    )
    lookup_field = 'uuid'

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action != 'retrieve':
            queryset = queryset.filter(user=self.request.user.pk).distinct()
        return queryset

    @swagger_auto_schema(request_body=PropertyToListingSerializer())
    @action(methods=['post'], detail=True, url_path='property')
    def add_real_property_to_listing(self, request, uuid):
        listing = Listing.objects.filter(uuid=uuid, user=self.request.user).first()
        real_property = RealProperty.objects.filter(pk=request.data.get('property_id')).first()
        if listing and real_property:
            listing.properties.add(real_property)
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(manual_parameters=[property_pk])
    @action(methods=['delete'], url_path='property/(?P<property_id>[^/.]+)', detail=True)
    def del_property_from_listing(self, request, uuid, property_id):
        listing = Listing.objects.filter(uuid=uuid, user=self.request.user).first()
        real_property = RealProperty.objects.filter(pk=property_id).first()
        if listing and real_property:
            listing.properties.remove(real_property)
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            instance.delete(force=True)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return super().destroy(request, *args, **kwargs)
