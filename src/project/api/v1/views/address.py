from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.permissions import AllowAny

from project.api.v1.filters import CountryFilterSet, CityFilterSet, DistrictFilterSet, AddressFilterSet
from project.api.v1.permissions import AdminPermissions, RealtorPermissions
from project.api.v1.viewsets import ExtendedModelViewSet
from project.apps.address.models import City, Address, District, Country
from project.api.v1.serializers.address import (
    CityListSerializer,
    CitySerializer,
    AddressListSerializer,
    AddressCreateUpdateSerializer,
    DistrictListSerializer,
    DistrictSerializer,
    CountryListSerializer,
    CountrySerializer
)

PERMISSION_MAP = {
    'create': AdminPermissions,
    'update': AdminPermissions,
    'partial_update': AdminPermissions,
    'list': AdminPermissions | RealtorPermissions,
    'retrieve': AdminPermissions,
    'destroy': AdminPermissions,
}


class CountryViewSet(ExtendedModelViewSet):
    """
    View for working with country data.

    create:
    Create new country.

    retrieve:
    Return the given country.

    list:
    List all countries.

    update:
    Update country data.

    partial_update:
    Partial update country data.

    delete:
    Mark country as deleted.
    """

    queryset = Country.objects.non_deleted()
    serializer_class = CountryListSerializer
    serializer_class_map = {
        'list': CountryListSerializer,
        'create': CountrySerializer,
        'update': CountrySerializer,
        'partial_update': CountrySerializer,
    }
    permission_map = {
        'create': AdminPermissions,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': AllowAny,
        'retrieve': AllowAny,
        'destroy': AdminPermissions,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = CountryFilterSet
    ordering_fields = ['name', 'created_at']


class CityViewSet(ExtendedModelViewSet):
    """
    View for working with city data.

    create:
    Create new city.

    retrieve:
    Return the given city.

    list:
    List all cities.

    update:
    Update city data.

    partial_update:
    Partial update city data.

    delete:
    Mark user as deleted.
    """

    queryset = City.objects.non_deleted()
    serializer_class = CitySerializer
    serializer_class_map = {
        'list': CityListSerializer,
    }
    permission_map = PERMISSION_MAP
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = CityFilterSet
    ordering_fields = ['name', 'created_at']


class DistrictViewSet(ExtendedModelViewSet):
    """
    View for working with district data.

    create:
    Create new district.

    retrieve:
    Return the given district.

    list:
    List all districts.

    update:
    Update district data.

    partial_update:
    Partial update district data.

    delete:
    Mark district as deleted.
    """

    queryset = District.objects.non_deleted()
    serializer_class = DistrictSerializer
    serializer_class_map = {
        'list': DistrictListSerializer,
    }
    permission_map = PERMISSION_MAP
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = DistrictFilterSet
    ordering_fields = ['name', 'created_at']


class AddressViewSet(ExtendedModelViewSet):
    """
    View for working with address data.

    create:
    Create new address.

    retrieve:
    Return the given address.

    list:
    List all addresses.

    update:
    Update address data.

    partial_update:
    Partial update address data.

    delete:
    Mark address as deleted.
    """

    queryset = Address.objects.non_deleted()
    serializer_class = AddressCreateUpdateSerializer
    serializer_class_map = {
        'list': AddressListSerializer,
        'retrieve': AddressListSerializer,
    }
    permission_map = PERMISSION_MAP
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = AddressFilterSet
    ordering_fields = ['postcode', 'created_at']
