from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, serializers, filters
from rest_framework.decorators import action
from rest_framework.response import Response

from project.api.v1.permissions import AdminPermissions
from project.api.v1.filters import UserFilterSet
from project.api.v1.serializers import BadRequestResponseSerializer
from project.api.v1.viewsets import ExtendedModelViewSet
from project.api.v1.serializers.user import (
    UserFullSerializer,
    UserCreateSerializer,
    UserUpdateSerializer,
    UserLoginSerializer,
    OneTimePasswordCreateSerializer,
    UserAccessTokenSerializer,
)

User = get_user_model()


class UserViewSet(ExtendedModelViewSet):
    """
    View for working with user data.

    retrieve:
    Return the given user.

    list:
    List all users.

    update:
    Update user data.

    partial_update:
    Partial update user data.

    delete:
    Mark user as deleted.

    login:
    Retrieve auth token by pair of username & password.

    sen_otp:
    Send email with OTP.

    my:
    Current use data.
    """

    queryset = User.objects.non_deleted()
    serializer_class = UserFullSerializer
    serializer_class_map = {
        'create': UserCreateSerializer,
        'update': UserUpdateSerializer,
        'partial_update': UserUpdateSerializer,
        'send_otp': OneTimePasswordCreateSerializer,
        'login': UserLoginSerializer
    }
    permission_map = {
        'create': permissions.AllowAny,
        'update': permissions.IsAuthenticated,
        'partial_update': permissions.IsAuthenticated,
        'list': AdminPermissions,
        'retrieve': AdminPermissions,
        'destroy': AdminPermissions,
        'login': permissions.AllowAny,
        'send_otp': permissions.AllowAny,
        'my': permissions.IsAuthenticated,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = UserFilterSet
    ordering_fields = ['role', 'created_at']

    def get_queryset(self):
        queryset = super().get_queryset()
        if AdminPermissions().has_permission(self.request, self):
            return queryset
        if self.action in ('update', 'partial_update'):
            queryset = queryset.filter(pk=self.request.user.pk)
        return queryset

    @action(methods=['get'], detail=False, filterset_class=None)
    def my(self, request, pk=None, **kwargs):
        return Response(self.get_serializer(instance=self.get_queryset().get(id=request.user.id)).data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False)
    def send_otp(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: UserAccessTokenSerializer})
    @action(methods=['post'], detail=False)
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(data=serializer.get_access(serializer.data), status=status.HTTP_200_OK)
