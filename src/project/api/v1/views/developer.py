from django.db.models import Q
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend

from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from project.apps.developer.models import Developer
from project.api.v1.filters import DeveloperFilterSet, UserFilterSet
from project.api.v1.permissions import AdminPermissions, DeveloperPermissions
from project.api.v1.serializers.developer import (
    DeveloperListSerializer,
    DeveloperCreateUpdateSerializer,
    DeveloperAvatarSerializer
)
from project.api.v1.viewsets import ExtendedModelViewSet
from project.api.v1.serializers import CommonErrorResponseSerializer
from project.api.v1.serializers.user import UserFullSerializer


class DeveloperViewSet(ExtendedModelViewSet):
    """
    View for working with developer data.

    create:
    Create new developer.

    retrieve:
    Return the given developer.

    list:
    List all developers.

    update:
    Update developer data.

    partial_update:
    Partial update developer data.

    delete:
    Mark developer as deleted.

    avatar:
    Create new developer avatar.
    """

    queryset = Developer.objects.non_deleted()
    serializer_class = DeveloperCreateUpdateSerializer
    serializer_class_map = {
        'list': DeveloperListSerializer,
        'retrieve': DeveloperListSerializer,
        'avatar': DeveloperAvatarSerializer
    }
    permission_map = {
        'create': AdminPermissions,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': (AdminPermissions | DeveloperPermissions),
        'retrieve': AdminPermissions,
        'destroy': AdminPermissions,
        'avatar': AdminPermissions
     }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = DeveloperFilterSet
    ordering_fields = ['name', 'created_at']

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        if AdminPermissions().has_permission(self.request, self):
            return queryset
        if DeveloperPermissions().has_permission(self.request, self):
            queryset = queryset.filter(Q(employees=self.request.user))
        return queryset.distinct()

    @swagger_auto_schema(responses={200: UserFullSerializer(many=True), 400: CommonErrorResponseSerializer})
    @action(methods=['get'], detail=True, url_path='employees', filterset_class=UserFilterSet)
    def employees(self, request, pk=None, **kwargs):
        self.filterset_class = UserFilterSet
        queryset = self.filter_queryset(self.get_object().employees.all())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(request_body=DeveloperAvatarSerializer(), responses={200: DeveloperAvatarSerializer()})
    @action(methods=['post'], url_path='avatar', detail=True, parser_classes=[MultiPartParser])
    def avatar(self, request, pk):
        serializer = DeveloperAvatarSerializer(get_object_or_404(Developer, pk=pk), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
