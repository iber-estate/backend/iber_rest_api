import logging

from django.db.models import Sum
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, filters
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from project.api.v1.filters import RealPropertyFilterSet
from project.api.v1.permissions import AdminPermissions
from project.api.v1.serializers.real_property import (
    RealPropertyListSerializer,
    RealPropertyAmenitiesListSerializer,
    TagsListSerializer,
    OwnershipTypesListSerializer,
    RealPropertyCreateUpdateSerializer,
    PhotoSerializer,
    DocumentSerializer,
    VideoSerializer,
    PhotoCreateSerializer,
    DocumentCreateSerializer,
    VideoCreateSerializer,
    PhotoOrderSerializer,
    SavedPropertyListSerializer,
    SavedPropertyCrateSerializer
)
from project.api.v1.viewsets import ExtendedModelViewSet
from project.apps.property_complex.models import PropertyComplex
from project.apps.real_property.models import (
    RealProperty,
    Amenities,
    OwnershipType,
    Tag,
    Photo,
    Video,
    Document,
    SavedProperty
)


class RealPropertyViewSet(ExtendedModelViewSet):
    """
    View for working with real property data.

    create:
    Create new real property.

    retrieve:
    Return the given real property.

    list:
    List all real property.

    update:
    Update real property data.

    partial_update:
    Partial update real property data.

    delete:
    Mark real property as deleted.

    amenities:
    List all amenities.

    tags:
    List all tags.

    ownership_types:
    List all ownership types.

    property_types:
    List all property types.

    add_photo:
    Add photo to real property.

    photo_order:
    Update photo order.

    add_video:
    Add video to real property.

    add_document:
    Add document to real property.

    del_photo:
    Mark real property photo as deleted.

    del_video:
    Mark real property video as deleted.

    del_document:
    Mark real property document as deleted.

    """
    queryset = RealProperty.objects.non_deleted().order_by('updated_at')
    serializer_class = RealPropertyListSerializer
    serializer_class_map = {
        'create': RealPropertyCreateUpdateSerializer,
        'update': RealPropertyCreateUpdateSerializer,
        'partial_update': RealPropertyCreateUpdateSerializer,
        'amenities': RealPropertyAmenitiesListSerializer,
        'tags': TagsListSerializer,
        'ownership_types': OwnershipTypesListSerializer,
        'add_photo': PhotoSerializer,
        'photo_order': PhotoOrderSerializer,
        'add_video': VideoSerializer,
        'add_document': DocumentSerializer,
        'save_property': SavedPropertyCrateSerializer
    }
    permission_map = {
        'create': AdminPermissions,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': permissions.AllowAny,
        'retrieve': permissions.AllowAny,
        'destroy': AdminPermissions,
        'amenities': permissions.AllowAny,
        'tags':  permissions.AllowAny,
        'property_types': permissions.AllowAny,
        'ownership_types': permissions.AllowAny,
        'add_photo': AdminPermissions,
        'photo_order': AdminPermissions,
        'del_photo': AdminPermissions,
        'add_video': AdminPermissions,
        'del_video': AdminPermissions,
        'add_document': AdminPermissions,
        'del_document': AdminPermissions,
        'save_property': permissions.IsAuthenticated,
        'del_saved_property': permissions.IsAuthenticated,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = RealPropertyFilterSet
    ordering_fields = ['price', 'created_at']
    is_saved = openapi.Parameter('is_saved', openapi.IN_QUERY, type=openapi.TYPE_BOOLEAN)

    def get_queryset(self):
        queryset = super().get_queryset()
        if AdminPermissions().has_permission(self.request, self):
            return queryset
        else:
            queryset = queryset.filter(status=RealProperty.Status.PUBLISHED)
        return queryset.distinct()

    @swagger_auto_schema(manual_parameters=[is_saved])
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            response = self.get_paginated_response(serializer.data)
            response.data['total_properties'] = PropertyComplex.objects.non_deleted().filter(
                pk__in=queryset.values_list('property_complex__pk')).aggregate(Sum('total_properties')).get('total_properties__sum') or 0
            return response

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=False, filterset_class=None)
    def amenities(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.get_serializer(self.paginate_queryset(Amenities.objects.all()), many=True).data)

    @action(methods=['get'], detail=False, filterset_class=None)
    def tags(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.get_serializer(self.paginate_queryset(Tag.objects.all()), many=True).data)

    @action(methods=['get'], detail=False, filterset_class=None)
    def property_types(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.paginator.paginate_queryset(
                [{'name': pt[1], 'slug': pt[0]} for pt in RealProperty.PropertyType.choices], request))

    @action(methods=['get'], detail=False, filterset_class=None)
    def ownership_types(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.get_serializer(self.paginate_queryset(OwnershipType.objects.all()), many=True).data)

    @swagger_auto_schema(request_body=PhotoCreateSerializer(), responses={200: PhotoSerializer()})
    @action(methods=['post'], url_path='photo', detail=True, parser_classes=[MultiPartParser])
    def add_photo(self, request, pk):
        photos = Photo.objects.non_deleted().filter(real_property__pk=pk)
        serializer = self.get_serializer(
            data={
                'real_property': pk,
                'photo': request.data.get('photo'),
                'order': photos.count() + 1 if photos else 1
            })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @swagger_auto_schema(request_body=PhotoOrderSerializer())
    @action(methods=['put'], url_path='photo_order', detail=True)
    def photo_order(self, request, pk):
        serializer = self.get_serializer(data={'sorted_photo_list': request.data.get('sorted_photo_list', {})})
        serializer.is_valid(raise_exception=True)
        order = 1
        sorted_photo_list = serializer.data['sorted_photo_list']
        if len(sorted_photo_list) != len(set(sorted_photo_list)):
            raise ValidationError({"sorted_photo_list": [f'must contain unique values']})
        for photo_id in sorted_photo_list:
            photo = Photo.objects.non_deleted().filter(pk=photo_id, real_property__pk=pk).first()
            if photo:
                old_photo = Photo.objects.non_deleted().filter(order=order, real_property__pk=pk).first()
                if old_photo:
                    old_photo.order = order + len(sorted_photo_list)
                    old_photo.save()
                photo.order = order
                photo.save()
                order += 1
            else:
                raise ValidationError(
                    {"sorted_photo_list": [f'photo with id {photo_id} not found for this real property']})
        return Response(status=status.HTTP_202_ACCEPTED)

    @action(methods=['delete'], url_path='photo/(?P<id>[^/.]+)', detail=False)
    def del_photo(self, request, id):
        photo = Photo.objects.non_deleted().filter(pk=id).first()
        if photo:
            photo.delete(force=True)
        return Response(status=status.HTTP_202_ACCEPTED if photo else status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(request_body=VideoCreateSerializer(), responses={200: VideoSerializer()})
    @action(methods=['post'], detail=True, url_path='video', parser_classes=[MultiPartParser])
    def add_video(self, request, pk):
        serializer = self.get_serializer(data={
            'real_property': pk,
            'external_url': request.data.get('external_url', ''),
            'video': request.data.get('video', None)
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(methods=['delete'], url_path='video/(?P<id>[^/.]+)', detail=False)
    def del_video(self, request, id):
        video = Video.objects.non_deleted().filter(pk=id).first()
        if video:
            video.delete()
        return Response(status=status.HTTP_202_ACCEPTED if video else status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(request_body=DocumentCreateSerializer(), responses={200: DocumentSerializer()})
    @action(methods=['post'], detail=True, url_path='document', parser_classes=[MultiPartParser])
    def add_document(self, request, pk):
        serializer = self.get_serializer(data={
            'real_property': pk,
            'external_url': request.data.get('external_url', ''),
            'file': request.data.get('file', None),
            'description': request.data.get('description')
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(methods=['delete'], url_path='document/(?P<id>[^/.]+)', detail=False)
    def del_document(self, request, id):
        document = Document.objects.non_deleted().filter(pk=id).first()
        if document:
            document.delete()
        return Response(status=status.HTTP_202_ACCEPTED if document else status.HTTP_404_NOT_FOUND)

    @action(methods=['post'], detail=True, url_path='save')
    def save_property(self, request, pk):
        real_property = RealProperty.objects.filter(pk=pk).first()
        if not real_property:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = self.get_serializer(data={'real_property': real_property.pk})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)

    @action(methods=['delete'], url_path='saved', detail=True)
    def del_saved_property(self, request, pk):
        real_property = RealProperty.objects.filter(pk=pk).first()
        if not real_property:
            return Response(status=status.HTTP_404_NOT_FOUND)
        saved_property = SavedProperty.objects.filter(real_property=real_property, user=self.request.user.pk).first()
        if not saved_property:
            return Response(status=status.HTTP_404_NOT_FOUND)
        saved_property.delete()
        return Response(status=status.HTTP_200_OK)


class SavedPropertyViewSet(ExtendedModelViewSet):
    queryset = SavedProperty.objects.all()
    serializer_class = SavedPropertyListSerializer
    serializer_class_map = {
        'list': SavedPropertyListSerializer,
        'create': SavedPropertyCrateSerializer,
    }
    ordering_fields = ['created_at']
    http_method_names = ['get', 'post', 'delete']

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user.pk)
        return queryset.distinct()

    def list(self, request, *args, **kwargs):
        self.request.user.new_notification = False
        self.request.user.save()
        return super().list(request, *args, **kwargs)
