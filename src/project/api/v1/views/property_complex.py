from django_filters.rest_framework import DjangoFilterBackend

from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, filters, status
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from project.api.v1.filters import PropertyComplexFilterSet
from project.api.v1.permissions import AdminPermissions
from project.api.v1.serializers.property_complex import (
    PropertyComplexListSerializer, PropertyComplexCreateUpdateSerializer, PropertyComplexAmenitiesListSerializer,
    PropertyComplexPhotoSerializer, PropertyComplexPhotoCreateSerializer, PropertyComplexPhotoOrderSerializer,
    PropertyComplexVideoLinkSerializer, DocumentLinkSerializer, DocumentLinkCreateSerializer,
    PropertyComplexVideoLinkCreateSerializer
)
from project.api.v1.serializers.property_cpmplex_detail import PropertyComplexDetailSerializer
from project.api.v1.viewsets import ExtendedModelViewSet
from project.apps.property_complex.models import PropertyComplex, Amenities, Photo, VideoLink, DocumentLink


class PropertyComplexViewSet(ExtendedModelViewSet):
    """
    View for working with property complex data.

    create:
    Create new property complex.

    retrieve:
    Return the given property complex.

    list:
    List all property complexes.

    update:
    Update property complex data.

    partial_update:
    Partial update property complex data.

    delete:
    Mark property complex as deleted.

    amenities:
    List all amenities.

    complex_types:
    List all complex types.
    """
    queryset = PropertyComplex.objects.non_deleted()
    serializer_class = PropertyComplexListSerializer
    serializer_class_map = {
        'create': PropertyComplexCreateUpdateSerializer,
        'update': PropertyComplexCreateUpdateSerializer,
        'partial_update': PropertyComplexCreateUpdateSerializer,
        'list': PropertyComplexListSerializer,
        'amenities': PropertyComplexAmenitiesListSerializer,
        'retrieve': PropertyComplexDetailSerializer,
        'add_photo': PropertyComplexPhotoSerializer,
        'photo_order': PropertyComplexPhotoOrderSerializer,
        'add_video': PropertyComplexVideoLinkSerializer,
        'add_document': DocumentLinkSerializer,
    }
    permission_map = {
        'create': AdminPermissions,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': permissions.AllowAny,
        'retrieve': permissions.AllowAny,
        'destroy': AdminPermissions,
        'amenities': permissions.AllowAny,
        'complex_types': permissions.AllowAny,
        'add_photo': AdminPermissions,
        'photo_order': AdminPermissions,
        'del_photo': AdminPermissions,
        'add_video': AdminPermissions,
        'del_video': AdminPermissions,
        'add_document': AdminPermissions,
        'del_document': AdminPermissions,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = PropertyComplexFilterSet
    ordering_fields = ['name', 'created_at']

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @action(methods=['get'], detail=False, filterset_class=None)
    def amenities(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.get_serializer(self.paginate_queryset(Amenities.objects.all()), many=True).data)

    @action(methods=['get'], detail=False, filterset_class=None)
    def complex_types(self, request, pk=None, **kwargs):
        return self.get_paginated_response(
            self.paginator.paginate_queryset(
                [{'name': pt[1], 'slug': pt[0]} for pt in PropertyComplex.ComplexType.choices], request))

    @swagger_auto_schema(
        request_body=PropertyComplexPhotoCreateSerializer(), responses={200: PropertyComplexPhotoSerializer()})
    @action(methods=['post'], url_path='photo', detail=True, parser_classes=[MultiPartParser])
    def add_photo(self, request, pk):
        photos = Photo.objects.filter(property_complex__pk=pk)
        serializer = self.get_serializer(
            data={
                'property_complex': pk,
                'photo': request.data.get('photo'),
                'order': photos.count() + 1 if photos else 1
            })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @swagger_auto_schema(request_body=PropertyComplexPhotoOrderSerializer())
    @action(methods=['put'], url_path='photo_order', detail=True)
    def photo_order(self, request, pk):
        serializer = self.get_serializer(data={'sorted_photo_list': request.data.get('sorted_photo_list', {})})
        serializer.is_valid(raise_exception=True)
        order = 1
        sorted_photo_list = serializer.data['sorted_photo_list']
        if len(sorted_photo_list) != len(set(sorted_photo_list)):
            raise ValidationError({"sorted_photo_list": [f'must contain unique values']})
        for photo_id in sorted_photo_list:
            photo = Photo.objects.filter(pk=photo_id, property_complex__pk=pk).first()
            if photo:
                old_photo = Photo.objects.filter(order=order, property_complex__pk=pk).first()
                if old_photo:
                    old_photo.order = order + len(sorted_photo_list)
                    old_photo.save()
                photo.order = order
                photo.save()
                order += 1
            else:
                raise ValidationError(
                    {"sorted_photo_list": [f'photo with id {photo_id} not found for this real property']})
        return Response(status=status.HTTP_202_ACCEPTED)

    @action(methods=['delete'], url_path='photo/(?P<id>[^/.]+)', detail=False)
    def del_photo(self, request, id):
        photo = Photo.objects.filter(pk=id).first()
        if photo:
            photo.delete()
        return Response(status=status.HTTP_202_ACCEPTED if photo else status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(
        request_body=PropertyComplexVideoLinkCreateSerializer(), responses={200: PropertyComplexVideoLinkSerializer()})
    @action(methods=['post'], detail=True, url_path='video', parser_classes=[MultiPartParser])
    def add_video(self, request, pk):
        serializer = self.get_serializer(data={
            'property_complex': pk,
            'video_link': request.data.get('video_link', ''),
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(methods=['delete'], url_path='video/(?P<id>[^/.]+)', detail=False)
    def del_video(self, request, id):
        video = VideoLink.objects.filter(pk=id).first()
        if video:
            video.delete()
        return Response(status=status.HTTP_202_ACCEPTED if video else status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(request_body=DocumentLinkCreateSerializer(), responses={200: DocumentLinkSerializer()})
    @action(methods=['post'], detail=True, url_path='document', parser_classes=[MultiPartParser])
    def add_document(self, request, pk):
        serializer = self.get_serializer(data={
            'property_complex': pk,
            'document_link': request.data.get('document_link', '')
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(methods=['delete'], url_path='document/(?P<id>[^/.]+)', detail=False)
    def del_document(self, request, id):
        document = DocumentLink.objects.filter(pk=id).first()
        if document:
            document.delete()
        return Response(status=status.HTTP_202_ACCEPTED if document else status.HTTP_404_NOT_FOUND)
