from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from project import celery_app
from project.api.v1.serializers.celery import BasicCeleryResultSerializer


class CeleryResultView(APIView):
    """View for celery result"""
    @swagger_auto_schema(responses={200: BasicCeleryResultSerializer})
    def get(self, request, pk):
        serializer = BasicCeleryResultSerializer(celery_app.AsyncResult(pk))
        return Response(serializer.data, status=status.HTTP_200_OK)
