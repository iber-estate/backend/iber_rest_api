from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from project.api.v1.serializers.suggest import SuggestSerializer


class SuggestView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = SuggestSerializer

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('input', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING)],
        responses={200: SuggestSerializer()})
    def get(self, request):
        serializer = self.serializer_class(data={'input': request.query_params.get('input', '')})
        serializer.is_valid(raise_exception=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
