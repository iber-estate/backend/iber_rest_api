import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from project.apps.core.redis import RedisManager
from project.apps.health.models import TestModel
from project.apps.health.tasks import celery_health_task


class HealthCheckView(APIView):
    """Backend health check"""

    permission_classes = [AllowAny]

    @staticmethod
    def _check_db():
        status = True
        try:
            obj = TestModel.objects.create(value="test")
            obj.title = "healthy"
            obj.save()
            obj.delete()
            logging.info('db health: ok')
        except Exception as e:
            logging.error(f'db health: {e.__class__.__name__} {e}')
            status = False
        finally:
            return status

    @staticmethod
    def _check_redis():
        status = True
        try:
            with RedisManager() as redis:
                redis.set("health", "ok")
                redis.delete("health")
            logging.info('redis health: ok')
        except Exception as e:
            logging.error(f'redis health: {e.__class__.__name__} {e}')
            status = False
        finally:
            return status

    @staticmethod
    def _check_celery():
        status = True
        try:
            celery_health_task.apply(kwargs={'data': 'celery-health-task'})
            logging.info('celery health: ok')
        except Exception as e:
            logging.error(f'celery health: {e.__class__.__name__} {e}')
            status = False
        finally:
            return status

    def get(self, _):
        logging.info("health-check:: requested")
        return Response(data={'status': 'ok'}, status=200) if all([
            self._check_db(),
            self._check_redis(),
            self._check_celery()
        ]) else Response(status=500)
