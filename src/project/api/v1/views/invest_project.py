from django.db import models
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_auto_schema

from rest_framework import filters, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from project.api.v1.filters import InvestProjectFilterSet
from project.api.v1.permissions import AdminPermissions
from project.api.v1.serializers.invest_project import (
    InvestProjectListSerializer,
    InvestProjectCreateUpdateSerializer,
    InvestProjectPhotoSerializer,
    InvestProjectPhotoCreateSerializer,
    InvestProjectPhotoOrderSerializer,
    InvestDocumentLinkCreateSerializer,
    InvestDocumentLinkSerializer,
    InvestProjectVideoLinkCreateSerializer,
    InvestProjectVideoLinkSerializer,
    InvestProjectPresentationLinkCreateSerializer,
    InvestProjectPresentationLinkSerializer
)

from project.api.v1.viewsets import ExtendedModelViewSet
from project.apps.invest_project.models import InvestProject, Photo, DocumentLink, VideoLink, PresentationLink

User = get_user_model()


class InvestProjectViewSet(ExtendedModelViewSet):
    """
    View for working with country data.

    create:
    Create new invest project.

    retrieve:
    Return the given invest project.

    list:
    List all invest projects.

    update:
    Update invest project data.

    partial_update:
    Partial update invest project data.

    delete:
    Mark invest project as deleted.
    """

    queryset = InvestProject.objects.non_deleted()
    serializer_class = InvestProjectListSerializer
    serializer_class_map = {
        'create': InvestProjectCreateUpdateSerializer,
        'update': InvestProjectCreateUpdateSerializer,
        'partial_update': InvestProjectListSerializer,
        'add_photo': InvestProjectPhotoSerializer,
        'photo_order': InvestProjectPhotoOrderSerializer,
        'add_video': InvestProjectVideoLinkSerializer,
        'add_document': InvestDocumentLinkSerializer,
        'add_presentation': InvestProjectPresentationLinkSerializer
    }
    permission_map = {
        'create': AllowAny,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': AllowAny,
        'retrieve': AllowAny,
        'destroy': AdminPermissions,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = InvestProjectFilterSet
    ordering_fields = ['name', 'created_at']

    def get_queryset(self):
        queryset = super().get_queryset()
        if AdminPermissions().has_permission(self.request, self):
            return queryset
        else:
            queryset = queryset.filter(status=InvestProject.Status.PUBLISHED)
        return queryset.distinct()

    def _add_link_instance(self, data):
        if not AdminPermissions().has_permission(self.request, self) and \
                int(data['invest_project']) not in self.request.user.projects.all().values_list('pk', flat=True):
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def _delete_instance(self, instance_model: models.Model, pk):
        if AdminPermissions().has_permission(self.request, self):
            instance = instance_model.objects.filter(pk=pk).first()
        else:
            instance = instance_model.objects.filter(pk=pk, invest_project__user=self.request.user).first()
        if instance:
            instance.delete()
        return Response(status=status.HTTP_202_ACCEPTED if instance else status.HTTP_404_NOT_FOUND)

    def create(self, request):
        response = super().create(request)
        refresh = RefreshToken.for_user(User.objects.get(pk=response.data.get('user')))
        response.data['access'] = str(refresh.access_token)
        return response

    @swagger_auto_schema(
        request_body=InvestProjectPhotoCreateSerializer(), responses={200: InvestProjectPhotoSerializer()})
    @action(methods=['post'], url_path='photo', detail=True, parser_classes=[MultiPartParser])
    def add_photo(self, request, pk):
        photos = Photo.objects.filter(invest_project__pk=pk)
        return self._add_link_instance(data={
            'invest_project': pk,
            'photo': request.data.get('photo'),
            'order': photos.count() + 1 if photos else 1
        })

    @swagger_auto_schema(request_body=InvestProjectPhotoOrderSerializer())
    @action(methods=['put'], url_path='photo_order', detail=True)
    def photo_order(self, request, pk):
        serializer = self.get_serializer(data={'sorted_photo_list': request.data.get('sorted_photo_list', {})})
        serializer.is_valid(raise_exception=True)
        order = 1
        sorted_photo_list = serializer.data['sorted_photo_list']
        if len(sorted_photo_list) != len(set(sorted_photo_list)):
            raise ValidationError({"sorted_photo_list": [f'must contain unique values']})
        for photo_id in sorted_photo_list:
            photo = Photo.objects.filter(pk=photo_id, invest_project__pk=pk).first()
            if photo:
                old_photo = Photo.objects.filter(order=order, invest_project__pk=pk).first()
                if old_photo:
                    old_photo.order = order + len(sorted_photo_list)
                    old_photo.save()
                photo.order = order
                photo.save()
                order += 1
            else:
                raise ValidationError(
                    {"sorted_photo_list": [f'photo with id {photo_id} not found for this project']})
        return Response(status=status.HTTP_202_ACCEPTED)

    @swagger_auto_schema(
        request_body=InvestDocumentLinkCreateSerializer(), responses={200: InvestDocumentLinkSerializer()})
    @action(methods=['post'], detail=True, url_path='document', parser_classes=[MultiPartParser])
    def add_document(self, request, pk):
        return self._add_link_instance(data={
            'invest_project': pk,
            'document_link': request.data.get('document_link', '')
        })

    @swagger_auto_schema(
        request_body=InvestProjectPresentationLinkCreateSerializer(),
        responses={200: InvestProjectPresentationLinkSerializer()})
    @action(methods=['post'], detail=True, url_path='presentation', parser_classes=[MultiPartParser])
    def add_presentation(self, request, pk):
        return self._add_link_instance(data={
            'invest_project': pk,
            'presentation_link': request.data.get('presentation_link', '')
        })

    @swagger_auto_schema(
        request_body=InvestProjectVideoLinkCreateSerializer(), responses={200: InvestProjectVideoLinkSerializer()})
    @action(methods=['post'], detail=True, url_path='video', parser_classes=[MultiPartParser])
    def add_video(self, request, pk):
        return self._add_link_instance(data={'invest_project': pk, 'video_link': request.data.get('video_link', '')})

    @action(methods=['delete'], url_path='photo/(?P<pk>[^/.]+)', detail=False)
    def del_photo(self, request, pk):
        return self._delete_instance(instance_model=Photo, pk=pk)

    @action(methods=['delete'], url_path='document/(?P<pk>[^/.]+)', detail=False)
    def del_document(self, request, pk):
        return self._delete_instance(instance_model=DocumentLink, pk=pk)

    @action(methods=['delete'], url_path='presentation/(?P<pk>[^/.]+)', detail=False)
    def del_presentation(self, request, pk):
        return self._delete_instance(instance_model=PresentationLink, pk=pk)

    @action(methods=['delete'], url_path='video/(?P<pk>[^/.]+)', detail=False)
    def del_video(self, request, pk):
        return self._delete_instance(instance_model=VideoLink, pk=pk)
