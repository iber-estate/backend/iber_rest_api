from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend

from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.response import Response

from project.apps.agency.models import Agency
from project.api.v1.filters import AgencyFilterSet, UserFilterSet
from project.api.v1.permissions import AdminPermissions, RealtorPermissions
from project.api.v1.viewsets import ExtendedModelViewSet
from project.api.v1.serializers import CommonErrorResponseSerializer
from project.api.v1.serializers.agency import AgencyListSerializer, AgencySerializer
from project.api.v1.serializers.user import UserFullSerializer


class AgencyViewSet(ExtendedModelViewSet):
    """
    View for working with agency data.

    create:
    Create new agency.

    retrieve:
    Return the given agency.

    list:
    List all agencies.

    update:
    Update agency data.

    partial_update:
    Partial update agency data.

    delete:
    Mark agency as deleted.
    """
    queryset = Agency.objects.non_deleted()
    serializer_class = AgencySerializer
    serializer_class_map = {
        'list': AgencyListSerializer,
    }
    permission_map = {
        'create': AdminPermissions,
        'update': AdminPermissions,
        'partial_update': AdminPermissions,
        'list': (AdminPermissions | RealtorPermissions),
        'retrieve': AdminPermissions,
        'destroy': AdminPermissions,
    }
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = AgencyFilterSet
    ordering_fields = ['name', 'created_at']

    def get_queryset(self):
        queryset = super().get_queryset()
        if AdminPermissions().has_permission(self.request, self):
            return queryset
        if RealtorPermissions().has_permission(self.request, self):
            queryset = queryset.filter(Q(employees=self.request.user))
        return queryset.distinct()

    @swagger_auto_schema(responses={200: UserFullSerializer(many=True), 400: CommonErrorResponseSerializer})
    @action(methods=['get'], detail=True, url_path='employees', filterset_class=UserFilterSet)
    def employees(self, request, pk=None, **kwargs):
        agency = self.get_object()
        self.filterset_class = UserFilterSet
        queryset = self.filter_queryset(agency.employees.all())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
