"""
This is a django-split-settings main file.

For more information read this:
https://github.com/sobolevn/django-split-settings

"""
from project.settings.components import config
from split_settings.tools import optional, include


# Managing environment via DJANGO_ENV variable:
ENV = config('DJANGO_ENV', 'development')

base_settings = [
    'components/apps.py',
    'components/cache.py',
    'components/celery.py',
    'components/common.py',
    'components/database.py',
    'components/drf.py',
    'components/internationalization.py',
    'components/jwt.py',
    'components/logging.py',
    'components/mailgun.py',
    'components/media.py',
    'components/static.py',
    'components/swagger.py',
    'components/templates.py',

    # You can even use glob:
    # 'components/*.py'

    # Select the right env:
    'environments/{0}.py'.format(ENV),

    # Optionally override some settings:
    optional('environments/local.py'),
]

# Include settings:
include(*base_settings)
