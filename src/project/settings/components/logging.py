LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s\t[%(levelname)s]\t%(message)s',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
    },
    'loggers': {
        'django.utils': {
            'handlers': ['console']
        },
        'py.warnings': {
            'handlers': ['console']
        },
        '': {
            'handlers': ['console'],
            'level': 'DEBUG'
        },
    },
}