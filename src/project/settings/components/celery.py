from celery.schedules import crontab

CELERY_BROKER_URL = "redis://redis/14"
CELERY_RESULT_BACKEND = "redis://redis/15"
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_SERIALIZER = 'json'
