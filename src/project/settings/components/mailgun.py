from project.settings.components import config

MAILGUN_API_KEY = config('MAILGUN_API_KEY')
MAILGUN_DEFAULT_MAILBOX_NAME = 'no-reply'
MAILGUN_DOMAIN_NAME = 'mg.iber.homes'
MAILGUN_DEFAULT_SENDER_NAME = 'Iber APP'
EMAIL_DEFAULT_SUBJECT = 'Iber OTP'
