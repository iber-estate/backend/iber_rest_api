TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = False

LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('de', 'German'),
    ('en', 'English'),
    ('es', 'Spanish'),
    ('fr', 'French'),
]

LOCALE_PATHS = ('locale/',)
