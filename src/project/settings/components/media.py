from project.settings.components import BASE_DIR

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR.joinpath('media')

THUMBNAIL_DEFAULT_SIZE = '564x316'
THUMBNAIL_DEFAULT_QUALITY = 99
