from typing import Tuple

INSTALLED_APPS: Tuple[str, ...] = (
    # Default django apps:
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third-party
    'corsheaders',
    'rest_framework',
    'django_filters',
    'drf_yasg',
    'sorl.thumbnail',
    'leaflet',
    'djgeojson',

    # Project pps
    'project.apps.address',
    'project.apps.agency',
    'project.apps.developer',
    'project.apps.health',
    'project.apps.listing',
    'project.apps.property_complex',
    'project.apps.real_property',
    'project.apps.invest_project',
    'project.apps.user',
)
