from project.settings.components import BASE_DIR

STATIC_URL = f'/static/'
STATIC_ROOT = BASE_DIR.joinpath('static')
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
