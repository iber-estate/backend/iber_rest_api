CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_ADDITIONAL_FIELDS = {
    'ckeditor': ['django.forms.fields.CharField', {'widget': 'ckeditor.widgets.CKEditorWidget'}]
}
