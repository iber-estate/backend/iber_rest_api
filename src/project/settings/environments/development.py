# Setting the development:
import django

from project.settings.components.apps import INSTALLED_APPS
from project.settings.components.common import MIDDLEWARE
from project.settings.components.database import DATABASES
from project.settings.components.logging import LOGGING

DEBUG = True
TRUE_VALUES = ('True', '1', 'TRUE', 'true')

DATABASES['default'].setdefault('OPTIONS', {})['connect_timeout'] = 3
alternate_db = DATABASES['default'].copy()
alternate_db['TEST'] = {'MIRROR': 'default'}
DATABASES['alternate'] = alternate_db

# MIDDLEWARE += ('silk.middleware.SilkyMiddleware',)
# INSTALLED_APPS += ('silk',)

LOGGING['handlers']['console']['level'] = 'DEBUG'

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
CORS_ORIGIN_ALLOW_ALL = True
django.setup()
