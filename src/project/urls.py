from django.urls import path, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path(f'{settings.PROJECT_URI.replace("/", "")}/admin/', admin.site.urls),
    path(f'{settings.PROJECT_URI.replace("/", "")}/v1/', include(('project.api.v1.urls', 'api-v1')))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# if settings.DEBUG:
#     urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]
